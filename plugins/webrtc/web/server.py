#!/usr/bin/env python3

import ssl
from http.server import HTTPServer, SimpleHTTPRequestHandler

httpd = HTTPServer(("", 8000), SimpleHTTPRequestHandler)

httpd.socket = ssl.wrap_socket(
    httpd.socket, keyfile="key.pem", certfile="cert.pem", server_side=True
)

httpd.serve_forever()
