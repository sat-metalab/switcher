/*
 * This file is part of switcher-watcher.
 *
 * switcher-watcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "watcher.hpp"

#include "switcher/shmdata/caps/utils.hpp"

namespace fs = std::filesystem;

namespace switcher {
namespace quiddities {
SWITCHER_MAKE_QUIDDITY_DOCUMENTATION(Watcher,
                                     "watcher",
                                     "Directory watcher",
                                     "Watch a directory for shmdatas",
                                     "LGPL",
                                     "Francis Lecavalier");

const std::string Watcher::kConnectionSpec(R"(
{
"writer":
  [
    {
      "label": "all%",
      "description": "Data streams",
      "can_do": ["all"]
    }
  ]
}
)");

Watcher::Watcher(quiddity::Config&& conf)
    : Quiddity(std::forward<quiddity::Config>(conf), {kConnectionSpec}),
      Startable(this),
      directory_id_(pmanage<MPtr(&property::PBag::make_string)>(
          "directory",
          [this](const std::string& val) {
            directory_ = val;
            return true;
          },
          [this]() { return directory_; },
          "Directory Path",
          "Full path of the directory to watch",
          directory_)),
      create_dir_id_(pmanage<MPtr(&property::PBag::make_bool)>(
          "create_directory",
          [this](bool val) {
            create_dir_ = val;
            return true;
          },
          [this]() { return create_dir_; },
          "Create directory",
          "Create the watched directory if it doesn't exist",
          create_dir_)) {}

Watcher::~Watcher() { stop(); }

bool Watcher::start() {
  stop();
  // Arguments validation
  if (directory_.empty()) {
    sw_error("Directory path must not be empty");
    return false;
  }

  // Directory checking and creation
  DirectoryStatus status = dir_exists(directory_);
  if (status == DirectoryStatus::ERROR) {
    return false;
  }
  if (status == DirectoryStatus::IS_FILE) {
    sw_error("{} is a file, not a directory.", directory_);
    return false;
  }
  if (status == DirectoryStatus::ABSENT) {
    if (!create_dir_) {
      sw_error("Directory {} does not exist.", directory_);
      return false;
    }
    std::error_code ec;
    if (!fs::create_directories(directory_, ec)) {
      sw_error("Directory could not be created. Error: {}", ec.message());
      return false;
    }
    fs::permissions(directory_, fs::perms::all, fs::perm_options::replace);
  }

  // Launch periodic checking
  readEventsTask_ = std::make_unique<PeriodicTask<>>([this]() { watch_events(); },
                                                     std::chrono::milliseconds(500));

  return true;
}

bool Watcher::stop() {
  std::lock_guard<std::mutex>{followers_mutex};
  readEventsTask_ = nullptr;
  followers_.clear();
  return true;
}

void Watcher::watch_events() {
  std::lock_guard<std::mutex>{followers_mutex};
  // Check if a file was deleted
  auto it = followers_.begin();
  while (it != followers_.end()) {
    std::string shmpath = std::get<0>(*it);
    if (!fs::exists(fs::path(shmpath))) {
      // gets the dynamic swid from the tuple, unregister it from the forced_writer_shmpaths_
      // and unallocate it.
      auto swid_it = shmpath_swids_.find(shmpath);
      if (swid_it != shmpath_swids_.end()) {
        claw::swid_t dynamic_swid = swid_it->second;
        claw_.remove_forced_writer(dynamic_swid);
        claw_.remove_writer_from_meta(dynamic_swid);
      }
      it = followers_.erase(it);
    } else {
      ++it;
    }
  }

  // Check if a file was created
  for (auto& file : fs::recursive_directory_iterator(directory_)) {
    std::string path = file.path().string();
    if (std::find_if(followers_.begin(),
                     followers_.end(),
                     [path](const std::tuple<std::string, std::unique_ptr<shmdata::Follower>>& t) {
                       return std::get<0>(t) == path;
                     }) == followers_.end()) {
      if (fs::is_socket(file.path())) {
        create_follower(path);
      }
    }
  }
  return;
}

void Watcher::create_follower(std::string shmpath) {
  followers_.push_back(std::make_tuple(
      shmpath,
      std::make_unique<shmdata::Follower>(
          this,
          shmpath,
          nullptr,
          // this is the on_server_connected callback
          [this, shmpath](const std::string& data_type) {
            std::lock_guard<std::mutex> lock{meta_swid_mutex};
            // Allocates a dynamic swid.
            // We need to do this on_server_connected because
            // we need to know the data type to generate a connection
            // spec with a specific can_do.
            std::string category = shmdata::caps::get_category(data_type);
            std::string mime_type(data_type.begin(),
                                  std::find(data_type.begin(), data_type.end(), (',')));
            claw::swid_t meta_swid = claw_.get_swid("all%");
            // the second parameter is the claw::shm_spec_t struct
            // with the fields {label, description, can_do} where can_do is a bracket initialized
            // vector
            claw::swid_t dynamic_swid =
                claw_.add_writer_to_meta(meta_swid, {category, shmpath, {mime_type}}, shmpath);
            shmpath_swids_.emplace(shmpath, dynamic_swid);
          },
          nullptr,
          shmdata::Stat::kDefaultUpdateInterval,
          shmdata::Direction::writer,
          true)));
}

Watcher::DirectoryStatus Watcher::dir_exists(const std::string path) const {
  try {
    if (!fs::exists(path)) {
      return DirectoryStatus::ABSENT;
    }
    if (fs::is_directory(path)) {
      return DirectoryStatus::PRESENT;
    }
  } catch (const fs::filesystem_error& e) {
    sw_error("Error while checking for directory. Error: {}", e.what());
    return DirectoryStatus::ERROR;
  }
  return DirectoryStatus::IS_FILE;
}
}  // namespace quiddities
}  // namespace switcher
