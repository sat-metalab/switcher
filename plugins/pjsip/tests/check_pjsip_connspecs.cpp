/*
 * This file is part of switcher.
 *
 * switcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * switcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with switcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG  // get assert in release mode

#include <atomic>
#include <cassert>
#include <condition_variable>

#include "switcher/quiddity/basic-test.hpp"

static bool success = false;
static std::atomic<bool> do_continue{true};
static std::condition_variable cond_var{};
static std::mutex mut{};

void wait_until_success() {
  // wait 3 seconds
  unsigned int count = 30;
  while (do_continue) {
    std::unique_lock<std::mutex> lock(mut);
    if (count == 0) {
      do_continue = false;
    } else {
      --count;
      cond_var.wait_for(lock, std::chrono::seconds(1), []() { return !do_continue.load(); });
    }
  }
}

void notify_success() {
  std::unique_lock<std::mutex> lock(mut);
  success = true;
  do_continue = false;
  cond_var.notify_one();
}

int main() {
  {
    using namespace switcher;
    using namespace quiddity;
    using namespace switcher::quiddity::property;
    using namespace switcher::quiddity::claw;

    Switcher::ptr manager = Switcher::make_switcher("siptest", true);

    auto sip_qrox = manager->quiddities->create("sip", "sip", nullptr);
    assert(sip_qrox);
    auto sip_quid = sip_qrox.get();
    assert(sip_quid);

    // print connection specs json for sip quiddity
    std::cout << "connection specs for sip:" << std::endl;
    std::cout << sip_quid->conspec<MPtr(&InfoTree::get_copy)>()->serialize_json() << '\n';

    // verify sfid for audio
    auto sip_quid_audio_sfid = sip_quid->claw<MPtr(&quiddity::claw::Claw::get_sfid)>("audio%");
    assert(sip_quid_audio_sfid);

    // verify sfid for ltc-diff
    auto sip_quid_ltc_diff_sfid =
        sip_quid->claw<MPtr(&quiddity::claw::Claw::get_sfid)>("ltc-diff%");
    assert(sip_quid_ltc_diff_sfid);

    // verify sfid for encoded video
    auto sip_quid_encoded_video_sfid =
        sip_quid->claw<MPtr(&quiddity::claw::Claw::get_sfid)>("video-encoded%");
    assert(sip_quid_encoded_video_sfid);

    // verify sfid for OSC
    auto sip_quid_osc_sfid = sip_quid->claw<MPtr(&quiddity::claw::Claw::get_sfid)>("osc%");
    assert(sip_quid_osc_sfid);

    // verify sfid for MIDI
    auto sip_quid_midi_sfid = sip_quid->claw<MPtr(&quiddity::claw::Claw::get_sfid)>("midi%");
    assert(sip_quid_midi_sfid);

    // get a audiotestsrc
    auto audiotestsrc_quid = manager->quiddities->create("audiotestsrc", "writer", nullptr).get();
    assert(audiotestsrc_quid);

    // starts the audio.
    audiotestsrc_quid->prop<MPtr(&PBag::set_str_str)>("started", "true");

    // connects the quiddities
    auto res_id = sip_quid->claw<MPtr(&Claw::try_connect)>(audiotestsrc_quid->get_id());
    assert(res_id);

    // SIP Claw callbacks on_shmdata_connect and on_shmdata_disconnect are currently unimplemented.
    // no shmdata connection happens for the moment.
    // you still need to use PJCall::attach_shmdata_to_contact API.

    // and then disconnect
    assert(sip_quid->claw<MPtr(&Claw::disconnect)>(res_id));

  }  // end of scope is releasing the manager
  return 0;
}
