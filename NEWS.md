NEWS
====
Here you will find a high level list of new features and bugfixes for each releases.

switcher 4.2.0 (2024-07-23)
---------------------------------

A mostly invisible update that lays groundwork for future development. Terminate work on using Switcher.IO back-end as main Switcher launcher, remove all of remaining gsoap back-end and wrap switcher and swctl commands into switcherio pipx package. Build and test only for Ubuntu 22.04.

* Update lint job to use clang-format 14 and prebuilt docker image ([!648](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/648))
* Implements the connection spec inspection command in swctl ([!647](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/647))
* Adds a global connection state getter and connection events for follower quiddities ([!646](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/646))
* Add an ldconfig step and add a note if a user installation of pipx doesn't cut it ([!644](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/644))
* Complete Switcher.IO integration ([!640](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/640))
* Properly notifies clients on connection spec changes ([!635](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/635))

switcher 3.5.4 (2024-03-20)
---------------------------------

Improvements of video codecs, audio network payloading, NDI support and fixes for multiple crashes related to multi-threading.

 * 🐛 Fix multi-channel audio payloading and revert using rtpgstpay ([!638](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/638))
    * fix for Multi-channel audio regression using GStreamer 1.16+ ([#603](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/603))
 * Makes the watcher less crashy ([!632](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/632))
    * partial fix for 🐛 Switcher crash when an NDI source that is encoded by an nvenc stops transmiting ([#234](https://gitlab.com/sat-mtl/tools/switcher/-/issues/234)) and Changing resolution of remote NDI source while an NDI Input (attached to that source) is active crashes switcher ([#587](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/587))
 * Makes some connection specs less vague ([!633](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/633))
    * fixes sdiInput does not work with NdiOutput starting from 4.1.4-rc1 ([#592](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/592))
 * Notifies the clients for sfid changes in infotree ([!628](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/628))
 * Make the Ids helper class thread safe ([!627](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/627))
    * fixes Double free or corruption crash when receiving a SIP call ([#266](https://gitlab.com/sat-mtl/tools/switcher/-/issues/266))
 * 🏗️ Payload audio as PCM L24 or OPUS ([!627](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/627))
 * 🩹 Use short VBV buffer for h.264 encoders ([!626](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/626))
 * 🩹 Use h.264 constrained-baseline profile ([!625](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/625))
 * Handle null gstreamer property description ([!624](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/624))
 * 🔇 Mute more Switcher.IO logging ([!623](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/623))
 * ⬆️ Add gstnvcodec support to videnc ([!622](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/622))
 * 🩹 Disable auto starting external Jack server ([!621](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/621))
 * ⚰️ Remove jackserver quiddity ([!620](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/620))
    * fixes Linking jack and jackserver libraries in same Switcher process causes undefined behaviors ([#276](https://gitlab.com/sat-mtl/tools/switcher/-/issues/276))
 * Fixes a segfault when ICE fails while receiving a SIP call ([!619](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/619))
    * fixes 🐛 Infrequent segfaults in PJICEStreamTrans when receiving a SIP call ([#271](https://gitlab.com/sat-mtl/tools/switcher/-/issues/271))
 * Manage subscriptions for dynamic properties ([!604](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/604))
    * mitigates 🐛Every call to pySwitch::get_quid creates a new PyQuiddity which is then never deallocated ([#212](https://gitlab.com/sat-mtl/tools/switcher/-/issues/212))
 * Adds bundle loading to swctl ([!616](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/616))
 * Disable Jack tests in CI ([!614](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/614))
 * [threads] Fix crash caused by concurrent access to a std::string ([!617](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/617))
    * fixes Occasional segfault in check_threaded_wrapper ([#268](https://gitlab.com/sat-mtl/tools/switcher/-/issues/268))
 * Omnibus clang & ASAN bugfix MR ([!606](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/606))

switcher 3.5.3 (2023-12-05)
---------------------------------

Bug fixes of `v4l2src` properties lifecycle during runtime to allow better resolution and frame rate. Tweak of `pjsip` options to allow building switcher with GStreamer Cerbero SDK.

 * [mbag] Add necessary checks before dereferencing iterators ([!615](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/615))
 * 🔊 Display Switcher and GStreamer versions in log ([!608](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/608))
 * Lower default resolution and encoder bit rate ([!605](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/605))
 * ✨ Add `-rcN` to switcher version string
 * ➖ Disable pjsip opus build option ([!607](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/607))
 * ➖ Disable pjsip opencore-amr build option ([!603](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/603))
 * 🐛 Properly updates v4l2 properties instead of nuking and rebuilding them ([!602](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/602))

switcher 3.5.2 (2023-10-25)
---------------------------------

New improvements on Switcher.io and switcher that brings back the thumbnails feature in Scenic and improve the `glfwin` support with screen detection.

 * 🐛 Allow empty invoke args in swctl ([!601](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/601))
 * 👽️ Switcher.io swctl CLI client improvements and documentation ([!592](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/592))
 * 🐛 Fix timelapse quiddity last_image property not updated in infotree ([!594](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/594))
 * ✨ Adds glfwin monitor selection ([!597](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/597))
 * ✨ Bring back thumbnails ([!596](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/596))
 * ✨ Prevent user from setting osc port to well known port range ([!591](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/591))

switcher 3.5.1 (2023-06-26)
---------------------------------

New bug fixes and features by re-introducing a cli tool `swctl` for Switcher and adding UUIDs with the SIP quiddity.

 * 🧑 Add missing git blame ignore revs ([!595](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/595))
 * ✨ Use IDs instead of nicknames in sip ([!588](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/588))
 * 🙈 Ignore the formating and linting commits with a .git-blame-ignore-revs file ([!587](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/587))
 * 🚨 Lint all project files with pre-commit hooks ([!578](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/578))
 * ✨ Add minimal working swctl utility ([!584](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/584))

switcher 3.5.0 (2023-06-15)
---------------------------------

Introducing `Switcher 3.5.0`, the latest release featuring the stabilization of `switcherIO`, a WebSocket tool that supports the client `Scenic 4.1`.

### 🐛 Bug fixes
 * 🐛 Add more ungiled ([!586](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/586))
 * 🐛 Restart executor process on command line change ([!585](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/585))
 * 🐛 Fix signal handling in executor by removing it ([!581](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/581))
 * 🐛 Fix wrong file path for glfwin overlay text ([!579](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/579))
 * 🐛 Append contact name to sip sources ([!575](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/575))
 * 🐛 Fix missing NDI audio output ([!569](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/569))
 * 🐛 Fix session.load deadlock ([!565](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/565))

### ✨ New features and optimisations
 * ⚡ Wrap call to set_str_str with ungiled ([!583](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/583))
 * ⚡ Wrap pyFollowerClaw::disconnect with ungiled ([!582](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/582))
 * ✨ Specializes extshmsrc writer 'can_do' on shmdata connect ([!580](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/580))
 * 🎨 Replace nicknames by quiddity ids in load and get state ([!574](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/574))
 * ✨ Use uuids for quiddity ids ([!570](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/570))
 * ✨ Hardcode x264enc codec tune preset to zerolatency ([!573](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/573))
 * ✨ Add switcher.io protocol logging ([!567](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/567))
 * ⚡ Improve Switcher.IO teardown ([!572](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/572))

### ♻ Refactorings and breaking changes
 * ♻ Wrap pyswitch::remove call with ungiled ([!571](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/571))
 * ♻ Use kind indexes in nickname generation ([!568](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/568))

### ✅ Varia
 * 👷 Add Metalab mpa to gitlab ci in the develop branch ([!563](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/563))

switcher 3.4.0 (2023-02-01)
----------------------------------

This is an official release in the 3.4 stable series. This is also the first release from the Valorisation team!

🐛 Bug fixes:
- [Fix switcherio shared ptr leak](https://gitlab.com/sat-mtl/tools/switcher/-/commit/b547ce1a5d53c87765e2afdd6a741085e21b76f2)
- [Fix gstreamer issues in shmdata-to-jack](https://gitlab.com/sat-mtl/tools/switcher/-/commit/29a1d309bc20ae7802e9cb64ec10ead030da7c99)
- [Fix executor signal handling](https://gitlab.com/sat-mtl/tools/switcher/-/commit/ce3406d46ec136da40f9fb4b7319f81873fc234d)
- [Fix sip sources nicknames for bundles](https://gitlab.com/sat-mtl/tools/switcher/-/commit/deb88a66c248b8837d435401dcd80065e8780dd8)
- [Fix test session gstreamer logic](https://gitlab.com/sat-mtl/tools/switcher/-/commit/c005f5881f5233938499633363d046caa41fd75f)
- [Fix broken state getter and setter to save and load connections](https://gitlab.com/sat-mtl/tools/switcher/-/commit/e42bcc90408f90b6293b43056d05f43f99bcf3be)
- [Fix return none when none is passed to get_quid_id](https://gitlab.com/sat-mtl/tools/switcher/-/commit/80eb43cc968b9bae3ef30df6b9f09239d7622519)
- [Fixes shmpath of extshmsrc when used with claw](https://gitlab.com/sat-mtl/tools/switcher/-/commit/8f37bd7b375083cca6d82a743b502b059ddb2e58)
- [Fix claw not honoring the "any" can_do value](https://gitlab.com/sat-mtl/tools/switcher/-/commit/78da89b6881a3d381495bdc6fdd5599be62002c7)
- [Fix missing sfid in encoder quiddities](https://gitlab.com/sat-mtl/tools/switcher/-/commit/4843681f798e929fc41da47f8a8e1937b2b208bc)
- [Fix base64_decode missing two chars](https://gitlab.com/sat-mtl/tools/switcher/-/commit/73092808344a383015559879f552d1ff2263a5a3)
- [Fix info tree not updated with property values](https://gitlab.com/sat-mtl/tools/switcher/-/commit/6427c5f6b5540ca7418a9908d52855a6af19261e)
- [Use asyncio threadsafe for all swio callbacks](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=4fd1ed782b5e60b972647b2c05631642f6aab7c0)
- [Fix Invalid sfid Bug](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=7be1a353dfa5673886fee2a284a111ef3ba09d7b)
- [Fix the check_configuration test when a global config file is present](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=73eb2ef6119613ea0f085cd3b31bc276b6f5a04a)
- [Fix the return value of user_tree_grafted so that it returns complex objects](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=5f063e7056b9aa86b7fe2519ce34ad689b1b29b8)
- [Fix global config file loading regression](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=5eb58229130bfb9a9e512a0fc33d6aad536c8852)

✨ New features:
- [Add connection specs on watcher](https://gitlab.com/sat-mtl/tools/switcher/-/commit/63bc34dcec76c37e74200f0ad25e5b0b48790347)
- [Add protect quiddities method](https://gitlab.com/sat-mtl/tools/switcher/-/commit/4f0e242e479525cf6b1a1ada872130ca2b61aafa)
- [Add connection validation and safeguards to claw](https://gitlab.com/sat-mtl/tools/switcher/-/commit/3c7798bd457a50d33a571c9d0929b1f02975e808)
- [Allow more than one writer connection spec in bundle](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=48c8422e8507abcddd7e8422f92febdfc3fb3da8)
- [Add follower connection specs to SIP quiddity](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=5eb27461333a716d8722c1b352e46c5b0d6db5e1)
- [Allow more than one follower connection spec in bundle](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=fe56df59863d006c7dbc76dc05d836117d15d872)

📝 Documentation
- [Adds some documentation and examples for InfoTrees](https://gitlab.com/sat-mtl/tools/switcher/-/commit/57c554333d7678801a385258de1bcf51e77e7a2f)

✅ Varia
- [Log socketio events only when the debug flag is specified](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=3cf70a9f3bb7f800d93de2daef011a9b67a19e12)
- [Move creation test faking out of test::create and into specific tests](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=0c4c4e15a1400653c661d3d10367e1da34cc0de2)
- [Remove obsolete and incorrect test in check_midi_plugins.cpp](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=719af276c8d8e91672a3e411b5f3718c632b2637)
- [Keep CTest logs as CI artifacts](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=ec714643a4456fd65abbd8a17b8c82916ceaf1b0)
- [Pass maintenance to the Valorisation department](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=4d7dbd33f0629c32fe3e09b48b7efe48d66128fc)

💥 Breaking changes
- [When removing a quiddity, disconnect every quiddity connected to it](https://gitlab.com/sat-mtl/tools/switcher/-/commit/69bba499f879d7843a2dfd0ee3acc430299f1af7)
- [Always publish stat updates](https://gitlab.com/sat-mtl/tools/switcher/-/commit/c5087922c2f4253f5a27f3ff037c87af113ac746)
- [Refactor toggle_property_saving into set_property_saving](https://gitlab.com/sat-mtl/tools/switcher/-/commit/b547ce1a5d53c87765e2afdd6a741085e21b76f2)
- [Set SIP extshmsrc nickname to "<media-label>"](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/545/diffs?commit_id=deb88a66c248b8837d435401dcd80065e8780dd8)
- [Return existing quiddity in swio create_quiddity](https://gitlab.com/sat-mtl/tools/switcher/-/commit/da1d083c119c7aa04cdd6a05f371d2af42fbb3a8)

switcher 3.3.0 (2022-12-12)
---------------------------
This is an official release in the 3.3 stable series.

Bug fixes:
* Fix/log must not be singleton
* Pyquid: fix segfault when Switcher is created without a name
* Stop sending poly aftertouch on midi sinc start
* Fix the prune API in swIO and add tests on top of this API
* Fix executor Shmdata specs
* Fix links in readme

New feature:
* Feat/surface sfid in Quiddity Infotree, under the Shmdata key

Documentation
* Documentation migrations from 3.2 to 3.3

switcher 3.2.0 (2022-11-17)
---------------------------
This is an official release in the 3.2 stable series.

Bug fixes:
* fix segfault in pyquid.InfoTree.prune
* fix swio session load, read, write
* fix RTMP claw
* fix parsing error with portmidi claw

New features:
* enable switcherio for Ubuntu 22.04

Improvements:
* add bus_info suffix to v4l2 card name
* activate the parsing error option for InfoTree in pyquid
* improve infotree logging of parsing eerrors


switcher 3.1.14 (2022-10-17)
---------------------------
This is an official release in the 3.1 stable series.

Bug fixes:
* 🐛 fix shmshot python error when display is requested
* 🐛 fix Use-After-Free JSON corruption in pySwitch.load_bundles
* 🐛 pyquid: fix invocation of a non existant method
* Replace PyThreadState with PyGILState API in non-Python created callback threads
* 🐛 fix nvenc test with the forcing of consistent I420 pixel format as default for videotestsrc

New feature:
* Add swsip-reflector service
* ✨ add the session api
* ♻️ add a descriptor for quiddities to pySwitch
* ♻️ add the get_description method to Quiddity

Improvements:
* Release id when nickname unavailable in Container::quiet_create
* Handle and display qrox error in pyquid.Switcher.create
* 👷 remove WebRTC test from CI
* :loud_sound: Fix a typo in container.hpp logger
* 📝 improve swIO troubleshooting with pdb
* 🔨 add a debug option on top of the swio cli
* 🚚 import and use pyquid types
* 🔊 add debug logs when swio is running
* ♻️ make the pyQuiddity ctor wrapping a Quiddity pointer
* 👷 update packaging script for packaging through Metalab MPAs

switcher 3.1.12 (2022-09-19)
---------------------------
This is an official release in the 3.1 stable series.

New features:
* ✨ add apt depedency file for nvidia on Ubuntu 22.04
* ✨ add connection spec callbacks in swIO

Improvements:
* 📝 update URLs for Scenic repositories
* 📝 update with new gitlab URL for Switcher
* 🍱 name for Vanshita Verma in the authors script

switcher 3.1.10 (2022-09-06)
---------------------------
This is an official release in the 3.1 stable series.

Bug fixes:
* 👷 be more lenient with shmdata delay test for CI performances
* 🐛 fix pyquid session segfault
* fix gst_element_get_request_pad not available after gstreamer version 1.20
* 🐛 fix claw description for dynamic shmdata writer filled with label instead of description
* 🐛 fix default configuration is lost when configuration is loaded from a file
* 👷 disable WEBRTC plugin and SWITCHERIO build and test for Ubuntu 22.04

New Features:
* ⬆️  port to Ubuntu 22.04
* ✨ add a query argument on top of the user_tree and info_tree APIs in swIO
* ✨ add a disconnect api for the quiddity

Improvements:
* add build Docker image for dependencies in Ubuntu 22.04, and build debian package for ubuntu 22.04
* 🔧 add a cmake option for Switcher IO, and remove forced installation of SwitcherIO python dependencies during cmake configuration
* remove deprecated and useless PyEval_InitThreads
* removed asyncio.get_event_loop in webrtc python tests
* add gstreamer1.0-nice inubuntu 22.04  dependency
* fix deprecation warning with asyncio
* 👷 add 22.04 into CI
* ✅ check_configuration ensures shm.prefix and log configuration are not lost after configuration reload
* shmshot: do not quit in callback

Documentation:
* 🚸 add an example that registers to sip
* 📝 add doc for build and install for Ubuntu 22.04
* updated runtime deps for ubuntu 22.04

switcher 3.1.8 (2022-07-26)
---------------------------
This is an official release in the 3.1 stable series.

Bug fixes:
* ✅ fix make package_source_test for pyquid not loading module when not installed
* ✏️fix typo for shmdata path description
* 🐛 fix audio ring buffer no updating available size when dest is nullptr
* 🚑️fix missing include in audio-ring-buffer

New features:
* ✨ add print infotgree in swquidinfo
* ✨ add read_sample to the audio ring buffer class
* ✨ add set-before option in swquid-infop

Documentation:
* 📝 add doxygen doc for AudioRingBuffer, AudioResample, DriftObserver, PulseSink and ShmdataToJack
* 📝 doxygen for selection (type of Quiddity property)
* 📝 add doc for AudioCaps

Improvements:
* ⚡️moved audio ring buffer, drif observer and resampler from jack plugin to utils. Use libsamplerate instead of custom resampling
* ⚡️pulsesink uses shmdata, pulse low level API and Switcher classes for audio drift correction
* ⚡️code simplification of async tasks in threaded wrapper
* ✨ SwitcherIO: API improvements
* ✨ improve quiddity creation from swio
* 📝 update doxygen file
* 📦 update nvidia keys


switcher 3.1.6 (2022-06-15)
---------------------------
This is an official release in the 3.1 stable series.

Bug fixes:
* 🐛 Fix midisrc claw cannot be parsed
* 🐛 Fix connection spec not set in v4l2src
* 🐛 Fix configuration loading
* 🐛 Fix glfwin sometimes freeze during destruction

Improvements:
* ✅ Add unit tests for `pyquid` using `unittest`
* 👷 update nvidia/cudagl image for CI builds
* 👷 update nvidia public key in CI

switcher 3.1.4 (2022-05-02)
---------------------------
This is an official release in the 3.1 stable series.

New features:
* ✨ Add STUN/TURN support to webrtc quiddity

Bug fixes:
* Fix webrtc receives now more than one room participant
* Fix webrtc regression
* Fix webrtc freezin audio shmdata when only audio is added to the session
* Fix RFC tempate typos
* Fix pyquid subscribe to user-tree signal remain silent

switcher 3.1.2 (2022-03-21)
---------------------------
This is an official release in the 3.1 stable series.

New Feature:
* Feat/Add CrashTest quiddity

Bug fixes:
* Fix missing gstreamer1.0-nice runtime dep for webrtc
* Fix docker login in CI
* Fix SIP transport not recreated when setting port property

Improvements:
* No stdin for docker login in CI
* Removed build of VRPN client and server, only library is built
* Update logging to new logger methods

switcher 3.1.0 (2021-12-13)
---------------------------
This is an official release in the 3.1 stable series.

New feature:
* ♻️ add `logs` configuration settings and replace logging system by `spdlog`

Bug fix:
* ✏️ Fix typo in property description

Improvements:
* 📝 upgrade doxygen file and exclude unrelated external code from doc
* Fix #40 The order of cd in INSTALL.md isn't right
* ⬆️  upgrade python websockets dependency for webrtc

switcher 3.0.4 (2021-11-29)
---------------------------
This is an official release in the 3.0 stable series.

Bug fix:
* Set SO_REUSEADDR in soap-ctrl-server and close socket in switcher-ctrl

switcher 3.0.2 (2021-11-01)
---------------------------
This is an official release in the 3.0 stable series.

New features:
* ✨ SwitcherIO: starting Websocket API for Switcher
* ✨ Add a configuration key for socket folder
* ✨ bring back shmpath option into switcher-ctrl

Bug fixes:
* 🐛 fix crash when trying to scan non existing directory during quiddity loading
* 🐛 fix config set_value not applyed
* 🐛 fix switcher not destroyed with python wrapper

Improvements:
* 👷 separate build and test
* 🎨 better typing for handling PyList size
* 👷 let `ctest` output on test failure
* Fix usage info typo in switcher-ctrl
* 💬 fix typos in the pjsip quiddity
* 🐛 fix dependency image in CI not updated

switcher 3.0.0 (2021-10-04)
---------------------------
This is an official release in the 3.0 stable series.

New features:
* ✨ InfoTree API: Usage improvements
* ✨ add Claw classes and integrate in Quiddity
* ✨ add can_do methods to claw and connect_quid
* ✨ add can_sink_caps to claw
* ✨ add connection API for shmdata reader Quiddity
* ✨ add dynamic shmdata in claw
* ✨ add get_copy to infoTree
* ✨ add repeat_array_indexes option in infoTree json serialization
* ✨ add signals for dynamic connection spec
* ✨ add try-connect and get-con-spec to switcher-ctrl, with updated doc
* ✨ get optional parsing error message when deserializing InfoTree
* ✨ parsing ConnectionSpec
* ✨ python wrapper for Claw

Bug fixes:
* 🎨 reorder python example files
* 🐛 fix CI
* 🐛 fix assert not handled in python text number 11
* 🐛 fix infotree foreach breaking the tree
* 🐛 fix shmshot (wrong use of python condition variable)

Improvements:
* 👷 add color to compiler output
* 📝 formating in Ids doxygen

Documentation:
* 📝 update MIGRATIONS.md for Switcher 3.0.0 release

Breaking change:
* 🐛 Fix the SIP plugin along with the `get_info` method for `pyquid`
* 💥 pyquid sends Infotree in signals rather than a json serialization of it
* 💥 removed connect methods on quiddities
* 💥 use kind keyword instead of either class or type for string description of quiddity types
* 🔥 remove category and tag


switcher 2.3.0 (2021-09-20)
---------------------------
This is an official release in the 2.3 stable series.

Documentation:
* 📝 Add MIGRATIONS.md
* 📝 Update author script
* 📝 Update configuration and bundle documentation

New features:
* ✨ Add Session API to Python wrapper
* ✨ Add IP address for each interface in systemusage's InfoTree
* ✨ Add print switcher version to swquid-info
* ✨ Add possibility to construct Quiddity object in python wrapper

Bug fixes:
* 🐛 Fix webrtc deadlock
* 🐛 Fix `pyquid.Switcher.get_quid` for Quiddity initialization
* 📝 Fix and improve doc
* 🐛 Fix defaul SIP port is applied before the one requested in configuration


Breaking changes:
* 🔥 Remove the syphon plugin
* 🔥 Remove Ubuntu 18.04 related files
* 💥 For consistency with the CPP API, refactor Quiddity.init and create in pyquid with name arg reworded as nickname
* add PyErr in pyquid.Quiddity

Improvements:
* 🚨 Reduce noise for pyquid at compile time

switcher 2.2.6 (2021-05-17)
---------------------------
This is an official release in the 2.2 stable series.

Improvements:
* ⬆️  upgrade dependency to nvidia driver version 460
* 📝 add gitlab issues template, code of conduct and RFC process

switcher 2.2.4 (2021-05-03)
---------------------------
This is an official release in the 2.2 stable series.

* ✨ Add hardware support for Nvidia Jetson

switcher 2.2.2 (2021-04-19)
---------------------------
This is an official release in the 2.2 stable series.

New feature:
* ✨ default plugin path is scanned recursively

Improvement:
* 🎨 install nvidia plugin in its own folder

switcher 2.2.0 (2021-04-08)
---------------------------
This is an official release in the 2.2 stable series.

Breaking changes, API refactoring:
* 💥 pyswitch on-quiddity-created and on-quiddity-removed callbacks give quiddity id instead of quiddity name
* 💥 removed pyqrox, pyswitch creates now directly a pyquiddity
* 💥 removed get_qrox_from_nickname
* 💥 get_name moved to get_id (Quiddity class), get_name moved to get_nickname and get_qrox_from_name moved to get_qrox_from_nickname (python)

Improvement:
* ⚡️ improved use of reference and const

switcher 2.1.38 (2021-04-08)
---------------------------
This is an official release in the 2.1 stable series.

* 🐛 Fix shmpath collision in PJSIP

switcher 2.1.36 (2021-03-23)
---------------------------
This is an official release in the 2.1 stable series.

New feature:
* ✨ Add allowlist_caps property in shmdelay

Improvements:
* ✅ Improved pyquid subscribe to property test
* 💬 Upgrade nvenc property descriptions
* 📄 Removed extra license

switcher 2.1.34 (2021-03-08)
---------------------------
This is an official release in the 2.1 stable series.

New Feature:
* Add a Ubuntu packaging script

Improvements:
* ✅ Improved python tests about signal subscription
* 🎨 Remove property refresh in videotestsrc
* ✨ Add can-sink-caps method to SIP quiddity

Bug Fixes:
* 💚 Fixed docker deploy CI for scenic
* 📝 Fixed docstring of Pyquid set_nickname
* 🐛 Fixed decodebin-to-shmdata issue with non-interleaved audio
* 🐛 Fixed jacksink quiddity segfault with non interleaved audio
* 🐛 Fixed VRPN not using the submodule

switcher 2.1.32 (2021-02-22)
---------------------------
This is an official release in the 2.1 stable series.

New features:
* ✨ Add subscribe and unsubscribe PySwitch methods
* ✨ Update PythonAPI to include load_bundles

Improvement:
* 👷 Replaced GLFW, ImGUI and PJSIP downloads with submodules
* ♻️  Get_quiddity_id returns a qid_t

Bug Fixes:
* 🐛 Fix make package_source_test to include non-root directories named build
* 🐛 Fix release script to get submodules
* 🐛 Fix call with shmdata with no switcher-name and quiddity-id in caps
* 🐛 Fix call with nvenc

switcher 2.1.30 (2021-02-09)
---------------------------
This is an official release in the 2.1 stable series.

New feature:
* ✨ Make the RTMP quiddity startable

Improvement:
* ✨ Allow whitespaces in nicknames sent to SIP

Bug fixes:
* 🐛 Fixed shmdata2jack not attaching to the jack dummy server
* 🐛 Fixed release script options
* 🐛 Fixed xvfb installation for CI tests

switcher 2.1.28 (2020-12-03)
---------------------------
This is an official release in the 2.1 stable series.

Bug fix:
* 🐛 fix ladspa quiddity issue limiting to mono input ladspa plugins

New features:
* ✨ Add server_name & client_name props in jacksink
* ✨ Add server_name property in jacksrc

Improvements
* 🎨 Rename jack-client-name property to client_name
* ➕ Add back swh-plugins dependency

switcher 2.1.26 (2020-11-16)
---------------------------
This is an official release in the 2.1 stable series.

Packaging and CI:
* 💚 fix check_switcher_ctrl with package_source_test
* 💚 fix pulsesrc test blocked when no pulseaudio server is running
* 👷 add Debian package build in CI with ubuntu 20.04
* 📦 generate Debian package with make package

Improvement:
* ♻️ Give IDs to shmpaths created by SIP quiddity

switcher 2.1.24 (2020-11-03)
---------------------------
This is an official release in the 2.1 stable series.

New feature:
* ✨ add support for SWITCHER_PLUGIN_PATH environment variable

Improvement:
* ⬇️  use dlopen instead of g_module for loading quiddity plugins

Bug fixes:
* 🐛 fix package_source_test when switcher is not already installed
* 🐛 fix segfault in avplayer when started with empty folder
* 👷 use dependency files for CI and install instructions
* 💚 fix glfw test frequent segfault in CI

switcher 2.1.22 (2020-10-05)
---------------------------
This is an official release in the 2.1 stable series.

* 💚 fix test-package (CI stage)

switcher 2.1.20 (2020-09-22)
---------------------------
This is an official release in the 2.1 stable series.

* ✨ Add quiddity and Switcher info in shmdata caps
* 🍱 updating switcher logo and adding graphic charter
* 📝 update dependencies in instalaltion doc (ubuntu 20.04)
* ✅ test for the webrtc quiddity

switcher 2.1.18 (2020-09-08)
---------------------------
This is an official release in the 2.1 stable series.

* 🎨 Refactored the simple server to be a class in the WebRTC Quiddity

switcher 2.1.16 (2020-08-07)
---------------------------
This is an official release in the 2.1 stable series.

* 📝 update instruction for build with ubuntu 20.04
* 🐛 fix filesrc quiddity blocking when set with a non existing file
* 🥅 catching errors related to method signatures in pyquiddity

switcher 2.1.14 (2020-07-14)
---------------------------
This is an official release in the 2.1 stable series.

* 🐛 Fix invalid quiddity pointer segfault

switcher 2.1.12 (2020-06-29)
---------------------------
This is an official release in the 2.1 stable series.

Bug fix:
* 🐛 compilation error with escape_json on arm

switcher 2.1.10 (2020-06-16)
---------------------------
This is an official release in the 2.1 stable series.

Bug fixes:
* 🐛 Fixed PJCall not handling correctly external shmdatas
* 🐛 fix swcam-display segfaut in 20.04

Dependency upgrades:
* ⬆️ upgrade glfw to version 3.3.2
* ⬆️ using Ubuntu 20.04 instead of 19.10 in CI for coverage

Improvements:
* ♻️ Add enums for various SIP statuses
* ♻️ use lambda instead of method pointer in ThreadedWrapper

switcher 2.1.8 (2020-06-01)
---------------------------
This is an official release in the 2.1 stable series.

* 📝 Improved Python wrapper documentation

switcher 2.1.6 (2020-05-19)
---------------------------
This is an official release in the 2.1 stable series.

Bug fixes:
* 🐛 fix issue with ladspa plugin with ca_FR locale
* 🐛 fix pyquid_quid_infotree_stress test

Improvements:
* 👽 Update PJSIP sendto function call to sendto2
* ✨ Expose SIP method to hang up all calls

switcher 2.1.4 (2020-05-06)
---------------------------
This is an official release in the 2.1 stable series.

New features:
* ✨ Add webrtc quiddity with example signaling server and web client

Bug fixes:
* 🐛 Graft only connections on attach_shmdata
* 🐛 Fixed release script not filling NEWS.md

New dependency related to the webrtc quiddity:
* ➕ Added libsoup for websocket communication
* ➕ Added gsteamer-webrtc-1.0


switcher 2.1.0 (2020-03-26)
---------------------------
This is an official release in the 2.1 stable series.

Documentation:
* Add an example of quiddity using gst

New features:
* ✨ Add dynamic bundle loading
* ✨ add cfor_each_in_array for infotree
* ✨ add safe-bool-log util
* ✨ adding shmshot tool for image capture of video shmdata

Bug fixes:
* 🐛 Fix a segfault when a glfwin is destroyed
* 🐛 Fix parsing of bundle parameters with dashes
* 🐛 Fixed CI fail on develop and master branch
* 🐛 Fixed an issue with dependencies detection through pkg-config
* 🐛 Return false when the pipeline's state can't be changed
* 🐛 fix jacksink not reconnecting to its autoconnect destination
* 🐛 improving glibmainloop concurrency strategy
* 🐛 urisrc unregisters uridecodebin signals before destruction of pipeline

Continuous integration:
* ✅ Allowed for GLFW plugin to be tested by the CI
* ✅ Changed Ubuntu version for coverage check
* ✅ added tests for tools

switcher 2.0.2 (2020-02-06)
---------------------------
This is an official release in the 2.0 stable series.

New features:
* ✨✅ adding videosnapshot quiddity

Bug fixes:
* 🐛  Fix deinterlace crash in v4l2src when pixel format is not raw video
* 💚 add missing dependency related to ubuntu 20:04 image update

Typos:
* ✏️  Fix typo in NEWS.md
* ✏️ sochet meant socket

switcher 2.0.0 (2020-01-31)
---------------------------
This is an official release in the 2.0 stable series.

Breaking changes:
* 💥 fileutils namespace
* 💥 any in switcher namespace
* 💥 namespace stringutils
* 💥 renaming logger classes and files
* 💥 renaming infotree files
* 💥 shmdata namespace
* 💥 quiddity namespace
* 💥 quiddities namespace
* 💥 log namespace
* 💥 gst namespace
* 💥 infotree namespace
* 💥 subfolder in switcher sources
* 💥 init_startable in now private, a StartableQuiddity must call the appropriate parent constructor
* 💥 Rename pixel_format property of videoconvert

New Features:
* ✨✅ adding for_each_in_array in InfoTree
* ✨ Add deinterlacer in v4l2src
* ✨ Add video properties auto-detect
* ✨ Add jack-server quiddity
* ✨ Add swquid-info, a command line informator for quiddities
* ✨ Add swcam-display tool
* ✨ Add graft Infotree by value
* ✨ Add get_name, get_type, set & get nickname in pyquiddity
* ✨ Add autostart property to midisink
* ✨ Add autostart property to OSCsink
* ✨ Add restart_on_change property to Executor
* ✨ Add do-lost in rtp-session
* ✨ Add video cropper quiddity

Bug fixes:
* 🐛 Fix SIP transmissin of shmdata created by NDI2Shmdata
* 🐛 Fix in shmdata-to-jack destruction
* 🐛 Fix play pause action in filesrc
* 🐛 Fix map midi value to property
* 🐛 Light refactor of OSCsrc
* 🐛 Fix handling of shmdata created by SIP when calling contact
* 🐛 Refactor gst-decodebin and fix erratic behavior when decoder connects to itself
* 🐛 Fix race condition with gstream pipeline play/pause
* 🐛 Explicitly capture invite_session in pjcall
* 🐛 Add a destructor for Watcher

Analytics:
* 📈 coverage in CI

Docs:
* 📝 Add GPLv3 badge in README.md
* 📝 Fix pipeline status in README.md
* 📝 Updated code structure documentation
* 📝 Updated InfoTree exemple links in writing-quiddity.md

Tests:
* ✅ Add test for switcher commands
* ✅ Add switcher log and quiddity configuration to quiddity-basic-test

switcher 1.1.2 (2019-09-17)
---------------------------
This is an official release in the 1.1 stable series.

New features:
* CI now pushes image into registry for develop and master branches + updated docker documentation
* Add quiddity (Watcher) for watching directories
* Add Executor quiddity
* addition of shmdata2jack, a simple command line tool.
* mention NDI2shmdata in README.md
* Add documentation on writing bundles
* Add run-switcher-in-docker in documentation
* Add missing caps specifier in JackToShmdata
* pjsip upgraded to 2.9
* Add argument to init ShmdataFollower tree on server connect
* Add do not convert rate and format in jacksink in order to get more than 63 channels
* Add stun turn port configuration in doc about bash scripting a sip call
* pyquid is installed in python lib path instead of cmake prefix
* removed nvdec quiddity
* Make nvdec (GStreamer element) compatible with decodebin and shmdata

Bug fixes:
* Fix: Resolve build problems from Scenic images
* Handle escaped whitespaces in bundle parser
* Fix signal handling problems in Executor
* Fix fullscreen windows not displaying in right monitor
* Fix GLFW fullscreen windows from minimizing automatically
* Escape special characters for JSON grafting
* Fix segfault when decorated glfwin is destroyed
* Fix fullscreen glfwin bug
* Fix dysfunctional image loading in glfwvideo
* adding Docker (desktop host and Raspi) file and documentation for shmdata, switcher & scenic in docker with support nvidia GPU encoding.
* less spelling mistakes
* adding informations about writing quiddities
* fix nvenc build on ubuntu 19.04
* fix race condition during GST Pipe destruction
* fix compilation issues with Ubuntu 19.04
* fix ltfdiff test sometimes crashing
* fix gsp-pipeliner leaking bus watch
* fixes in shmdata to jack
* do not notify reader stats
* fix set sip related server port from uris
* fix set sip server port from registration uri
* register to the appropriate quiddity in infotree stress python test
* fix inconsistent control of gst pipeline play/pause states
* document how to set remote sip server port from command line
* fix append of remote port in sip registration url
* fix compilation issue with pyquiddity.cpp
* use asyncio in pyquid infotree stress test
* fix possible deadlocks in pyquiddity
* typo in qrox header
* fix glfwin sometimes segfault
* glfwin handle dynamic change in caps without disconnecting shmdata
* avoid passing nullptr as a string in uridecodebin
* infotree signal stress test and pyquid multi-threading improvement
* pyquid: use save and restore thread instead of not thread safe PyGIL
* more asserts in pyquid signal file
* pixel converter destruct gst pipeline before elements
* gstpipe set gst pipeline to null from the destructor and unref from gmainloop
* gstpipe kills gst pipeline from the main loop
* fix sometimes filesrc crash at deletion (members order in quiddity-container)
* reverse gst-element ref removal from previous commit
* adding unsubscribe to signal in python exemple
* avoid segfaut due to simultameous g_signal_handler_disconnect and signal notification in gst-subscriber
* information tree serialization is mutexed now
* Add documentation for building NVDEC
* Make nvdec compatible with decodebin and shmdata
* Fix log messages in PJCall
* Removed the validation on overlay_additional_font and overlay_config properties
* fix member orders in videotestsrc
* adding python3 in CI and switcher dependencies
* Handle gstreamer.primary_priority configuration as an int
* Drop corrupted frames when decoding H264 streams
* Allow a Gstreamer plugin's rank to be set using the switcher.json file

switcher 1.1.0 (2018-12-13)
---------------------------
This is an official release in the 1.1 stable series.

New features:
* adding switcher logo
* dynamic configuration at quiddity creation that override switcher static configuration
* jack quiddities takes a dynamic configuration for selection of jack server name (see python example #8)
* adding invoke_async to pyquiddity

Bug fixes:
* check for availability of misleading indentation flag with c++ compiler
* check for availability of deprecated register for a jack header
* ltc plugin checks for availability of deprecated register for a jack header
* uint replaced by unsigned int
* fix clang compilation (but need to disable SOAP plugin)

switcher 1.0.0 (2018-09-19)
---------------------------
This is an official release in the 1.0 stable series.

Bug fixes:
* fix jacksink infinite loop
* saving shm connection is now based on reader saving name and suffix of writer quiddity if available. raw_shmdata path are still possibly saved if no information about the writer are available
* adding a test for shmdata transfert between bundles
* get header installed in a switcher version specific folder
* c++ version generated in pkgconfig
* fix Urisrc will sometimes crash on delete
* fix config not loaded excepted for bundles
* fix compilation for ubuntu 18.04

New Features:
* switcher python wrapper
* improved documentation
* new filesrc quiddity with play/pause/loop with basic test.
* adding two properties for jacksink: i) connect all to first channel and ii) connect only first channel
* httpsdpdec uses now a prop instead of a method for uri
* add log options to sip plugin, config option to python wrapper, and example python script
* adding initial doc for protocol mapper
* InfoTree copy
* get version and default plugin path from methods in switcher
* Makes it so videoconvert uses multi-threading if available (GStreamer version >= 1.12).
* add a resampling quiddity based on libsamplerate
* provide plugin path with pkgconfig

switcher 0.8.68 (2018-04-06)
---------------------------
This is an official release in the 0.8 stable series.

* Adding sip-call.md
* Add using-osc-quiddities.md
* Disable libwebrtc for pjsip because we don't use it and it prevents switcher from being built on ARM (no SSE2 support, duh!)

switcher 0.8.66 (2018-02-21)
---------------------------
This is an official release in the 0.8 stable series.

* fix the nvenc bug causing crash when adding/removing nvenc quiddities

switcher 0.8.64 (2017-11-29)
---------------------------
This is an official release in the 0.8 stable series.

* actually deactivated buggy lock win aspect ratio in glfw

switcher 0.8.62 (2017-11-27)
---------------------------
This is an official release in the 0.8 stable series.

* deactivated buggy lock win aspect ratio in glfw

switcher 0.8.60 (2017-10-27)
---------------------------
This is an official release in the 0.8 stable series.

Bug fixes:
* Fix member ordering issue with pjsip
* pjsip built in release mode with extra checks (assert does not quit anymore)
* Fix sdp issue with opus audio
* Extra check for pjsip ids
* Fix media-label containing some char results in stream not sent
* Change iv4l2 io-mode when using force framerate option to avoid being stuck.

New Features:
* Refactoring log system Renaming manager_impl_ into qcontainer_ QuiddityConfiguration for creation of quiddities Remove registry by quiddity name Init method removed from quiddities
* Ubuntu 17.04
* Timecode "package" and shmdelay documentation.
* Adding support for timecode input to compute delay automatically.
* Shmdata generic delay with manual delay.
* Timecode generation or reading timecode from source file.

switcher 0.8.58 (2017-08-14)
---------------------------
This is an official release in the 0.8 stable series.

Bug fixes:
* glfw win aspect ratio
* fix nicknames not saved
* fix audio property group not initialized into gst-audio-codec
* fixed selection properties not being able to be set correctly after selection refactor

New Features:
* shmdata AVPlayer (still seek/sync issues)
* signaling on-nicknamed
* rename quiddity-manager into switcher and quiddity manager impl into quiddity container
* make it so the periodic task starts at regular intervals instead of waiting a fixed period of time
* refactoring signals using native c++ instead of gobject
* centralize quiddities doc management outside of quiddity class

switcher 0.8.56 (2017-07-20)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* nickname for quid
* using quid nickname in sip calls
* Multichannel audio/video shmdata recorder.
* adding jacksink max_number_of_channels config
* adding window aspect ratio locking in glfwvideo
* reorder glfw groups
* adding max_number_of_channels config for jacksrc
* select selections by name is now available

Bug fixes:
* consistent property naming among jacksrc and jacksink
* refactoring quiddity manager: no more commands and simpler save and load
* prefer droping frame than accumulating in GLFWIN
* set drop-on-latency at true for rtpbin in rtp-session2
* fix audiotestsrc sometime crashing during check_startable
* fix midisrc always expose shmwriter
* Fixed protocol mapper CMakeList.
* fix SIP plugin with OSX

switcher 0.8.54 (2017-04-12)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* ENABLE_GPL is now available as a compilation option, see README for more details.

Bug fixes:
* OSX compilation.
* Save default instead of IP address when SIP DNS server is default.
* Fixes enabled/disabled state and message for width and height of videotestsrc.
* Force pixel-aspect-ratio in timelapse to fix deadlock when not providing one.
* Copy/paste error in audio shmdata subscriber pruning callback in RTMP quiddity.
* Fixes hardcoded channel mask for jacksrc shmdata.

switcher 0.8.52 (2017-03-31)
---------------------------
This is an official release in the 0.8 stable series.

Bug fixes:
* fix rtmp does not seem to connect to shmdata
* fix can-sink-cap brocken in scenic wyth extshm
* init tree with shmpath in extshmwriter
* update dependencies for OSX
* fix logs not displayed with switcher

switcher 0.8.50 (2017-03-17)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* RTMP quiddity compatible with multiple streaming applications (Youtube/Twitch tested).
* Defaults glfw window overlay to bottom center and 20 pt font size.
* Adding tree grafted and pruned to signal quid example.
* Remove visual disconnection message because it is very often misleading (until we have the reinvite feature).
* Synchronize SIP account de-registration.

Bug fixes:
* Fix race condition with shmdata subscribers in pjsip.
* Put the glClear back awaiting cleanup of the rendering loop because it caused issues with capture cards visualizations.
* Fixed fragmnent shader (caused intel/amd issues). Removed useless glClear causing flickering on slow computers.
* Fix sip label collision.
* Fix some incoming_call_ related crash when caller hangs up.

switcher 0.8.48 (2017-03-06)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* Generic property to protocol mapper. Two first protocols: OSC/Curl. Removed redundant http-get-service quiddity.
* Increase default time interval for the shmdata update process. Keep it shorter (1s) for discrete quiddities like osc/midi.
* LTC to jack transport quiddity.

Bug fixes:
* Only define default update interval for shmdata stats in one place and fix the displayed byte rate and rate.
* Fix GL dependencies in the install instructions
* Fixed appearance of glfw overlay.
* Close SIP transport when destroying pjsip threaded wrapper. Avoids having the sip port attached after closing switcher.
* Fixed and reorganized doxygen build.
* reinitialize jack_input when starting jack-to-shmdata

switcher 0.8.46 (2017-02-17)
---------------------------
This is an official release in the 0.8 stable series.

Bug fixes:
* update coding.md
* fix leaking extshmsrc

switcher 0.8.44 (2017-02-03)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* Added support for multichannel in ladspa plugin.

Bug fixes:
* Check weak ptr to shared ptr validity in pjsip.
* Various fixes in imgui and glfw.
* Setting of fullscreen property was desynchronized due to its processing in render loop.
* Fix jack trying to connect ports with quiddity name instead of jack client name.

switcher 0.8.42 (2017-01-20)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* Creating a quiddity for each incoming SIP stream is now the default behaviour.

Bug fixes:
* fix issue with unsubscribe signals of a quiddity
* Remove useless lock that could cause a deadlock.
* Put all geometry modifications in render task to avoid race conditions.
* Added a timeout to SIP account registration to avoid potential deadlock.
* Fixed wrong buddy id lookup in SIP plugin.
* Forward manager configuration to the bundle manager.
* Process position and size of glfwin in render loop and optimize property setters in case we set them without changing their value.
* Fixed deadlock during removal of exposed SIP quiddity.
* Checks validity of OpenGL buffers before deleting them. Doing otherwise could crash at destruction in some early error cases (e.g no display then the OpenGL API is not loaded and calling glDelete* would crash.)

switcher 0.8.40 (2017-01-04)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* New option in SIP configuration to automatically create a raw shmdata quiddity for incoming streams.
* New notification system in QuiddityContainer to track removal/creation of quiddities. SIP Plugin uses it instead of old legacy CreateRemoveSpy quiddity.
* Remove CreateRemoveSpy quiddity and all its hooks in QuiddityContainer_Impl.
* Port to nvidia SDK 7.1. Needs driver version 375.20 or newer.

Bug fixes:
* Fixed quiddity renaming helper, wrong string was returned.
* Fix midi not shown when receiving from sip.
* Fix member order issue at destruction in bundle class.
* Self destruct the bundle if one of its quiddity is destroyed.

switcher 0.8.38 (2016-12-14)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* New quiddity implementing LADSPA plugins.
* New whitelist mechanism when using no_prop option in bundle definition. reviewers: nouillot, edurand
* Added "group" keyword in bundle description to customize the name of the group of properties of each quiddity.

Bug fixes:
* Fixed nvenc test after bundle pipeline syntax change.
* Use docker image for CI to speed it up.
* Fixed a potential race condition between gst::Pipeliner main loop and its destructor.
* MIDI unit test does not fail if /dev/snd cannot be found.
* Fixed deadlock when deleting a badly created glfwin. Also, overlay is not mandatory now, if something wrong happens during overlay initialization, it will only be hidden.
* Unify OSC quiddity naming.
* Change git path to gitlab for release script.
* Forward graft/prune events in bundles for custom branches (e.g: focused event in glfwin).
* Keep bundle parameters ordered so that dependent properties are declared in the right order. reviewers: nbouillot, edurand
* Fixed properties ordering in bundle for grouping to avoid losing properties in the inspector.
* Added a safety in bundle property mirroring.

switcher 0.8.36 (2016-11-25)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* New bundle option to have a quiddity's properties at top level of the inspector. review: nbouillot
* Dummy sink and nvenc decode test with a bundle,

Bug fixes:
* Various fixes in nvenc, glfw and shmdata-to-jack.
* Bundle properties were not mirrored when creating dynamically after the creation of the bundle quiddity.
* Fixed build type for glfw and a memory leak in ImGui context creation.
* Graphical overlay in glfwin.
* gcc 6.4 on ubuntun 16.10 fixes.
* Removing hardcoded shmdata initial sizes.
* Fix segfault for receiver when cleaning a call.
* Fix issues with SIP when loading file (blacklisting several SIP methods).
* Optionally lower case accounts and TURN user.
* v4l2src USB devices are pushed at the end of the device list.

switcher 0.8.34 (2016-11-11)
---------------------------
This is an official release in the 0.8 stable series.

New Features:
* Add possibility of setting  desired target bitrate for NVENC.
* Allows blacklisting of param in a bundle without specifying its value.
* VRPN Properties Added support for exposing VRPN analog and button channels as properties in the source as well as creating custom analog and button properties to be exposed in the sink.

Bug Fixes:
* Synchronize shmdata version in switcher in release script.
* When saving history, only filter the quiddity creation step for quiddities_at_reset instead of ignoring them completely.
* Json parser was failing when parsing a null-value node. Also refactored deserializer to make it more consistent with json-glib API.
* Bundle internal manager is scaning same plugin dirs as the bundle manager.
* Add required dependency 'libssl-dev' to install cmd
* Property replace correctly notifies UI.
* Fixed race condition with ports_to_connect_ in shmdata-to-jack.
* Fixed missing property fields in tree when replacing the property.
* Fixed empty configuration tree issue.
* Fixed cmake build in release mode.
* Fixed parent property not working with bundles.
* Fixed underscores in bundle names causes shmdata path issues.
* Fixed v4l2 standard framerates not disabled when started.
* Fixed bundle does not disconnect shmdata.

switcher 0.8.32 (2016-10-28)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* Persistence when loading scenarii with pulse sources.
* Cmake build system.
* Fixed Mac build.
* Bundle: filter property per quiddit.y
* Startable bundle: quiddities can have their started property exposed at the bundle level.
* From bundle pipeline description, optionally hide all properties of a given quiddity.

switcher 0.8.30 (2016-10-21)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* GLFW window.
* Bundle implementation, forwarding properties from contained quiddities check if quid names are unique in bundle force name specification for each quid in a bundle bundle connects and disconnects shmdatas among contained quiddities.
* vrpn pluginm, source and sink.

Bug fixes:
* Fixed a crash when creating/removing sources from the system and disabled device selection when started.
* various fixes in shmdata-to-jack,
* videoconvert cannot connect to itself,
* adding jackd as dependency in INSTALL.md,

switcher 0.8.28 (2016-10-03)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* Using shmdata version 1.3,
* Adding empty quiddity for saving independant user tree,
* Custom framerates in v4l2src and videotestsrc,
* Videotestsrc and v4l2src do not compress,
* Add a mechanism to force the set callback of a property,
* Adding a message in infotree that tells why a property has been disabled,
* Sdd color property type,
* V4l2src is saving devices either by port, or by device id,
* Per quiddity custom state saving,
* Update INSTALL.md with additional Nvidia details,
* Added a mechanism to add a notification callback to a gstreamer property,
* Notify the information tree when a gtk window gets the focus,
* Porting from GTK2 to GTK3.

Bug fixes:
* Fixed version numbering when minor or major version update,
* Fix missing spaces in INSTALL.md,
* Fix gtk3 warning message during configure,
* Fix default video encoder cannot handle full HD,
* Fix quid issue with long names,
* Fix SIP remote contact added even if call is refused,
* Clear all artifacts when stopping or disconnecting the source from a gtk window or when rotating the image,
* Clang doesn't accept to friend "class" something defined as a struct. Fixed OSX build,
* Move include to fix build on OSX.

switcher 0.8.26 (2016-09-12)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* Shmdata access rate is notified in the quiddity information tree, along with byte_rate.
* Make DNS in SIP configurable and get the system one by default instead of a hardcoded value.
* Audiotestsrc quiddity revamping : added properties (sample rate, channels number, audio format), modified gstreamer pipeline lifecycle.
* Cleaned up UGstElem lifecycle (better refcount management) and fixed PBag::replace so it doesn't reset the index of the selection.
* Adding more properties to videotestsrc and templating selection.
* Removing more codec option from gst-video-codec and gst-audio-codec.
* Added generic methods to get gstreamer elements caps values in gst::utils.
* Adding SIP whitelist.
* Using nvenc 7.
* Only record the shmdata connections state instead of each connect/disconnect command when saving a session.

Bug fixes
* Fix stun/turn from configuration file is deadlocking.
* SIP quiddity notifies when configuration is applied at initialization.
* Fix bug regarding multiple simultaneous nvenc sessions crashing, re-enabled nvenc test, code simplication.

switcher 0.8.24 (2016-08-31)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* URI/URL image player.

Bug fixes:
* Fixed shmdata size computation and disconnection method.
* Clarified timeout warning for SIP registration and reduced invite timeout.
* Catch gstreamer errors in URI player to avoid deadlocks.
* Removing hardcorded nvidia driver version in release script.
* Adding audio multichannel error message for issue with encoder.

switcher 0.8.22 (2016-08-17)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* H265 encoding/decoding with NVENC/NVDEC.
* gst-audio-codec cleanup

Bug fixes:
* Added mutexes for safety and fixed a crash in jack_on_process.
* Fixed shmdata lifecycle in nvdec plugin to fix a race condition.
* No initialization of CUVIDDECODECREATEINFO because it contains elements withtout default constructor.

switcher 0.8.20 (2016-08-05)
---------------------------
This is an official release in the 0.8 stable series.

* fix jacksrc cannot connect to jacksink

switcher 0.8.18 (2016-08-04)
---------------------------
This is an official release in the 0.8 stable series.

* save file: save quiddities and property values and history is saving only invocations,
* alphanum, space and '-' are only allowed chars in quiddity names, others are replaced by '-',
* saving and loading user_data tree with history,
* fix OSX compilation,
* fix sometime wrong property (Selection) value when generated from GStreamer element enum property,
* fix property notification stops after property replace,
* fix 2 nvenc stress test,
* fix audio encoder default value,
* fix property documentation not updated with replace,
* fix gtkwin xevent to shmdata,
* fix a type mismatch in the sip_port property.

switcher 0.8.16 (2016-07-20)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* Implementation of hardware decoding with nvcuvid.
* Port to NVIDIA Codec SDK 6 and build cleanup.
* SIP quiddity exposes decompressed streams (default) or compressed streams,

Bug fixes:
* fix SIP status cannot go from offline to something else
* fix issues with video converter quid (making gst-pixel-format-converter RAII)
* fix nvenc test
* notifying user that no more nvenc quiddity can be created,
* increasing number of simultaneous media in SIP/SDP,

switcher 0.8.14 (2016-07-06)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* Fixed stun negotiation for SIP. NAT traversal works now
* c++14 support
* Posture_Source: added new filtering parameters (bilateral filter and hole filling)

Bug fixes:
* prevent self call through SIP
* fix sometimes failing check_rtp_session
* reduce registration timeout to 3 seconds when server name is wrong
* message to user when SIP port cannot be bound plus other messages
* add pixel-aspect-ratio to caps in posture_source.cpp
* fix v4l2src issue when error from the GStreamer pipeline

switcher 0.8.12 (2016-06-21)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* Generic decoder quiddity
* PostureScan3DGPU: added support for multicore compression,
* Reflecting modifications to PostureVision to support RS cameras,

Bug fixes:
* Fixed regex for version and set git merge strategy to 'theirs' in release script.
* Fix bug with threaded wrapper sync task
* jack fixes and jack server is starting if not running,
* using pjsip 2.5.1 instead of 2.2.1
* Created a script to parse and print translatable error messages from switcher to scenic.
* Use g_message to give information to the interface from switcher (temporary solution awaiting quiddity manager refactoring).
* Fix SIGSEGV and SIGABRT errors happening during property changes (race conditions).
* Hang up incoming call after outgoing call in case of self-call, otherwise shmdata would not be disconnected properly when hanging up.
* X events capture is now optional for gtk window.
* Disable nvenc options when an encoding session is ongoing.
* Manage error case in property subscription mechanism.

switcher 0.8.10 (2016-06-08)
---------------------------
This is an official release in the 0.8 stable series.

New features:
* GTK geometry saving
* Added per-quiddity config
* Per-quiddity user tree
* Posture: Added a compress option to Scan3DGPU
* Add autoconnect feature to jack source and output.
* Reformatted code base with clang-format.
* untested syphonsrc port to shmdata1
* Posture: added PostureColorizeGL

Bug fixes:
* do not call contact if already calling it
* Release script, usable by shmdata and switcher.
* fix nvenc 2Kx2K transmission with SIP
* adding a more complete test for nvenc plugin
* fix h264 in gst codec
* sip hang up is hanging up outgoing call or incoming call, but not both in the same invocation
* pjsip do not assert if cannot answer to call
* Fixed SIP connections graft/prune and empty array serialization for the tree
* Fixed an issue where only the last shmdata reader was updated in the info tree.
* Destroy "logger" quiddity last to keep proper formatting until the end.
* fix systemusage period property
* Fixed deadlocks and crashes when reloading SIP scenarii.
* Switch from hybrid sync/async messaging system in gst-pipeliner to using gstreamer watch mechanism.
* errors are fatal and crash the app (SIGTRAP), changed to a warning with error in the message.
* fix nvenc max number of simultaneous encode sessions reached
* Fixed a crash on disconnection of a source during a SIP call and an accidental creation of a reader key.
* fix video converter crash for large video,
* fix new call from already calling contact,
* fix load history file including create returning null
* Precommit hook now proposes to do the formatting automatically on the changes.
* Added PostureMeshSerialization quiddity
* Update video caps on-the-fly when selecting them in Scenic.
* fix switcher-ctrl, renaming get_factory_capabilities in get_classes,
* disabling soap set_port after first invocation
* v4l2 shmdata writer is initialized with a larger value in order to avoid automatic resize for the first frame.
* v4l2src uses video-encoded suffix when the device produces non raw video
* GCC 5.3 is more restrictive than 5.1 and is used in Ubuntu 16.04. Soap needed an additional warning exception to build properly.
* fix fullscreen button latency with gtkwin
* fix gtk win properties when no video is connected
* fix gtk window shortcuts
* fix jack-to-shmdata error when deleted
* fix issue in shmdata-to-jack
* fix issue when property not found with soap
* fix missing arg issue with switcher-ctrl
* make timelapse multi shmreader
* num files and dir and nome parameters for timelapse
* max files in timelapse
* timelapse params can be changed live
* width and height in timelapse
* timelapse notifies last image in a read only property
* fix default video encoding is ugly on ubuntu 15.10
* fix seg when trying to register with network and without STUN/TURN
* fix v4l2 trying to compress video when camera is producing compressed video
* fix SIP cannot send to multiple destinations
* fix SIP call issue when no media add to SDP
* fix gtk property issues
* fix nvenc bug when start/stop input video
* fix lost property values for video-codec when start stop
* fix v4l2src capture configuration lost after start/stop
* fix gtk dtor bug
* fix some timelapse internals
* fix distcheck with posture
* PostureScan3DGPU: added preliminary support for mesh refinement
* PostureScan3DGPU: updated some limit values
* Posture: now checking in PostureSource and PostureScan3DGPU that the calibration file is alright
* more changes trying to fix posture transmission
* (pjsip) ports only determined by ICE
* removing several FIXME in pj-sip plugins
* removing some FIXMEs
* some simplification of pjsip plugin code
* fix Stack smashing detected when using stun with 4 shmdata
* fix race condition in rtpsender
* fix generated quid names collision after loading
* fix issue with saving/loading
* fix SIP shmdata byte_rate monitoring
* fix sip compilation with g++ 5.2
* fix missing string with g_warnings
* fix conflict with posture plugin
* fix duplicate byte rate info for shmdata writer in httpsdpdec
* Improved posture plugin threading
* removing old sockfd fix for OSX
* fix pruning with clang
* activating syphon for being ported to shmdata1
* fix check startable
* fix race condition with GstSubscriber
* no wait for state when asking PLAYING or PAUSED state
* disabling GTK on OSX
* add missing include for errno
* fix clang issue with gchar vs. string
* removing curl dependency
* removed hard coded switcher version in switcher-nodejs

switcher 0.8.8 (2016-04-04)
---------------------------
This is an official release in the 0.8 stable series.

New features:

* video timelapse for multiple video shmdata
* option to number files or not
* max number of file for timelapse
* width and height in timelapse
* timelapse notifies last file written

Bugs fixed:

* default video encoding ugly on ubuntu 15.10
* segfault when trying to register with network and without STUN/TURN
* v4l2 trying to compress video when camera is producing compressed video
* SIP cannot send to multiple destinations
* SIP call issue when no media add to SDP
* gtk property issues
* nvenc bug when start/stop input video
* lost property values for video-codec when start stop
* v4l2src capture configuration lost after start/stop
* gtk dtor bug

switcher 0.8.6 (2016-03-21)
---------------------------
This is an official release in the 0.8 stable series.

New features:

* property system refactored
* posture GPU
* nvenc support
* STUN/TURN support
* video timelapse

Bugs fixed:

* too many to be listed here

switcher 0.8.4 (2015-08-18)
---------------------------
This is an official release in the 0.8 stable series.

New features:

* none

Bugs fixed:

* remove dependency to curl (not working with launchpad ppa)

switcher 0.8.2 (2015-08-17)
---------------------------
This is an official release in the 0.8 stable series.

New features:

* none

Bugs fixed:

* switcher plugin not loaded from nodejs addon

switcher 0.8.0 (2015-08-17)
---------------------------
This is an official release in the 0.8 stable series.

New features:

* SIP
* VNC (compatible with gtkwin)
* jack (resampling + autoconnection)
* audio encoder
* more option with v4l2 capture
* ported to gstreamer 1.0
* ported to shmdata 1.0
* using internal information tree

Bugs fixed:

* too many to be listed here

switcher 0.6.2 (2014-08-04)
---------------------------
This is an official release in the 0.6 stable series.

New features:

* none

Bugs fixed:

* nodejs not loading plugins

switcher 0.6.0 (2014-08-04)
---------------------------
This is an official release in the 0.6 stable series.

New features:

* information tree
* syphon (OSX)
* improved per-quiddity shmdata dynamic description
* shmdata-any support
* system usage quiddity
* SIP
* posture plugins

Bugs fixed:

* too many for being listed here

switcher 0.4.6 (2014-03-07)
---------------------------
This is an official release in the 0.4 stable series.

New features:

* none

Bugs fixed:

* node-switcher is better integrated with nodejs

switcher 0.4.4 (2014-03-05)
---------------------------
This is an official release in the 0.4 stable series.

New features:

* adding node-switchon addons in sources

Bugs fixed:

* fix segfault in soap-ctrl-client
* make node-switcher encoding strings in utf8 instead of ascii

switcher 0.4.2 (2014-02-27)
---------------------------
This is an official release in the 0.4 stable series.

New features:

* title property for gtkvideosink
* adding videoflip, video balance and gamma to gtk video sink

Bugs fixed:

* too many for being listed here

switcher 0.4.0 (2014-01-28)
---------------------------
This is an official release in the 0.4 stable series.

New features:

* many video encoders are available with video-sources
* dynamic presence of properties
* property mapper
* supporting plugins
* gtk video sink with fullscreen
* portmidi plugins
* pulse plugins
* v4l2 plugins
* dictionnary
* osc controler
* several internal features for developing quiddities
* no need to invoke set_runtime anymore

Bugs fixed:

* too many for being listed here

switcher 0.2.2 (2013-06-20)
---------------------------
This is an official release in the 0.2 stable series.

New features:

* none

Bugs fixed:

* gsoap packaging

switcher 0.2.0 (2013-06-19)
---------------------------

This is the first official release in the 0.2 stable series.

New features:

* signals
* save/load command history
* soap client

Bugs fixed:

* too many to listed here

switcher 0.1.2 (2013-04-26)
---------------------------
This is the first developer snapshot of switcher.
