# This file is part of switcher python wrapper.
#
# libswitcher is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General
# Public License along with this library; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place, Suite 330,
# Boston, MA 02111-1307, USA.

import logging
import unittest

import pyquid


class ResetStateTestCase(unittest.TestCase):
    logger = logging.getLogger(__name__)
    logging.basicConfig(
        level=logging.DEBUG, format="%(name)s: %(funcName)s: %(message)s"
    )

    @classmethod
    def setUpClass(cls):
        # initialize switcher
        global sw
        sw = pyquid.Switcher("pyquid", debug=True)

    def test_protect_and_reset(self):
        sw.create(kind="dummysink", nickname="dummy1")
        sw.create(kind="dummysink", nickname="dummy2")
        self.assertEqual(len(list(sw.quiddities)), 2)
        # remove all non protected
        sw.reset_state()
        self.assertEqual(len(list(sw.quiddities)), 0)
        assert sw.create(kind="dummysink", nickname="dummy1") is not None
        assert sw.create(kind="dummysink", nickname="dummy2") is not None
        # consider all currently existing quiddities to be protected
        sw.protect_quiddities()
        sw.create(kind="dummysink", nickname="dummy3")
        # remove all non protected
        sw.reset_state()
        self.assertEqual(len(list(sw.quiddities)), 2)
        d3 = sw.create(kind="dummysink", nickname="dummy3")
        sw.create(kind="dummysink", nickname="dummy4")
        d5 = sw.create(kind="dummysink", nickname="dummy5")
        # protects d3 and d5 additionnaly
        sw.protect_quiddities([d3.id(), d5.id()])
        sw.reset_state()
        self.assertEqual(len(list(sw.quiddities)), 4)

    def test_invalid_use_cases(self):
        # tries some invalid use cases of protect_quiddities
        try:
            sw.protect_quiddities([34])
        except Exception as e:
            self.assertTrue(type(e) is TypeError)
