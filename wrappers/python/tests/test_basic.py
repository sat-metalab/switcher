# This file is part of switcher python wrapper.
#
# libswitcher is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General
# Public License along with this library; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place, Suite 330,
# Boston, MA 02111-1307, USA.

import logging
import unittest

import pyquid


class BasicTestCase(unittest.TestCase):
    logger = logging.getLogger(__name__)
    logging.basicConfig(
        level=logging.DEBUG, format="%(name)s: %(funcName)s: %(message)s"
    )

    @classmethod
    def setUpClass(cls):
        # initialize switcher
        global sw
        sw = pyquid.Switcher("pyquid")
        # create a window
        try:
            cls.window = sw.create(kind="glfwin", nickname="win1")
        except RuntimeError as e:
            cls.logger.exception(e)
            cls.window = sw.create(kind="dummysink", nickname="win1")
        cls.dummy = sw.create("dummy", "dummy")
        # create a video test source
        cls.video_quid = sw.create("videotestsrc", "vid")

    @classmethod
    def tearDownClass(cls):
        # remove existing quiddities
        for quid in sw.quiddities:
            sw.remove(quid.id())

    def test_switcher_name(self):
        self.assertEqual("pyquid", sw.name())

    def test_switcher_version(self):
        self.assertTrue(sw.version())

    def test_quiddities(self):
        vid = BasicTestCase.video_quid
        self.assertTrue(vid.nickname())
        self.assertTrue(vid.get_kind())
        nickname = "vid1"
        vid.set_nickname(nickname)
        self.assertEqual(nickname, vid.nickname())

    def test_get_quid_by_id(self):
        vid = BasicTestCase.video_quid
        vid_nick = vid.nickname()
        vid_id = vid.id()
        self.assertEqual(vid_id, sw.get_quid_id(vid_nick))
        self.assertEqual(None, sw.get_quid_id("IDoNotExist"))
        self.assertEqual(None, sw.get_quid_id(None))

    def test_properties(self):
        vid = BasicTestCase.video_quid
        # test properties
        self.assertTrue(vid.get_info_tree_as_json(".property"))
        propdoc = pyquid.InfoTree(vid.get_info_tree_as_json(".property"))
        self.assertFalse(propdoc.empty())
        self.assertTrue("started" in propdoc.get_key_values("id", False))
        self.assertFalse(vid.get("started"))

        # sets the bool_ property of the dummy quid.
        # by default it is True
        BasicTestCase.dummy.set("bool_", False)
        # the bool_ property from the info tree should equal the bool_ property
        # that we get directly.
        self.assertEqual(
            BasicTestCase.dummy.get_info(".property")[0]["value"],
            BasicTestCase.dummy.get("bool_"),
        )

    def test_connect(self):
        win, vid = BasicTestCase.window, BasicTestCase.video_quid
        # quiddities can be connected together using `shmdata`
        try:
            sfid = win.try_connect(vid)
        except RuntimeError as e:
            self.logger.exception(e)

        self.assertTrue(sfid.disconnect())
