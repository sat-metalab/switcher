# This file is part of switcher python wrapper.
#
# libswitcher is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General
# Public License along with this library; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place, Suite 330,
# Boston, MA 02111-1307, USA.

import json
import logging
import unittest
from copy import deepcopy
from subprocess import Popen
from time import sleep

import pyquid


def compare_session_serializations(session_one, session_two):
    """
    This function compares two session serialization but does not take the generated ids and the order of
    the data into account.
    returns True if the two serializations are semantically identical.
    """
    # we convert values() to list because the dict_value object does not implement __eq__ but list does.
    quids_are_the_same = list(session_two["quiddities"].values()) == list(
        session_one["quiddities"].values()
    )
    properties_are_the_same = list(session_two["properties"].values()) == list(
        session_one["properties"].values()
    )
    user_data_are_the_same = list(session_two["userdata"].values()) == list(
        session_one["userdata"].values()
    )
    # matches the quiddity ids in session_one and session_two by looking at matching kinds and nicknames

    # Explanation of the dict comprehension expression below :
    # Create a dict that associates the sess_one_id and the sess_two_id
    # where sess_one_id and sess_two_id are taken from the ids that are the keys
    # of the quiddities dict of both session object. Do this association only if the object at those keys are identical.

    # In other words, create a mapping of which ids correspond to the same quiddities in two different session objects.
    matching_ids = {
        sess_one_id: sess_two_id
        for sess_one_id in session_one["quiddities"]
        for sess_two_id in session_two["quiddities"]
        if session_one["quiddities"][sess_one_id]
        == session_two["quiddities"][sess_two_id]
    }
    # creates a list of connections with the ids translated from the session_one ids to the session_two ids.
    translated_session_one_connections = []
    for connection in session_one["connections"]:
        translated_connection = deepcopy(connection)
        translated_connection["follower"]["id"] = matching_ids[
            connection["follower"]["id"]
        ]
        # if the connection has a writer object (meaning it is not a connection to a raw shmpath)
        # translates that too
        if "writer" in connection:
            translated_connection["writer"]["id"] = matching_ids[
                connection["writer"]["id"]
            ]
        translated_session_one_connections.append(translated_connection)
    connections_are_the_same = (
        translated_session_one_connections == session_two["connections"]
    )
    return all(
        [
            quids_are_the_same,
            properties_are_the_same,
            user_data_are_the_same,
            connections_are_the_same,
        ]
    )


class SessionTestCase(unittest.TestCase):
    logger = logging.getLogger(__name__)
    logging.basicConfig(
        level=logging.DEBUG, format="%(name)s: %(funcName)s: %(message)s"
    )

    @classmethod
    def setUpClass(cls):
        # initialize switcher
        global sw
        sw = pyquid.Switcher("pyquid", debug=True)

    def remove_all_quids(self):
        for qid in sw.list_ids():
            sw.remove(qid)

    def test_save_and_load_session(self):
        # give alllllll the diff.
        self.maxDiff = None
        # create 3 sources 2 destinations and one property-quid(dummy) and connect them
        dummy_quid_1 = sw.create(kind="dummysink", nickname="dummy1")
        dummy_quid_2 = sw.create(kind="dummysink", nickname="dummy2")
        vid_quid_1 = sw.create(kind="videotestsrc", nickname="vidsrc1")
        vid_quid_2 = sw.create(kind="videotestsrc", nickname="vidsrc2")

        meta_follower_quid = sw.create(
            kind="dyn-reader-quid", nickname="metaquid"
        )
        property_quid = sw.create(kind="dummy", nickname="propquid")
        # fills the user tree of the property quid with test values.
        # These should be serialized and reinstated on load
        prop_quid_user_tree = property_quid.get_user_tree()
        prop_quid_user_tree.graft("valeur", 1)
        prop_quid_user_tree.graft("valeurs.nested.float", 3.0)
        prop_quid_user_tree.graft("valeurs.nested.int", 42)
        prop_quid_user_tree.graft("valeurs.nested.string", "Konono No. 1")
        # creates an array in the tree. The keys "premier" "deuxième" [...] will be lost in
        # the process but the values should be reinstated on load
        prop_quid_user_tree.graft("konono1.albums.premier", "Luabuku")
        prop_quid_user_tree.graft("konono1.albums.deuxième", "Congotronics")
        prop_quid_user_tree.graft(
            "konono1.albums.troisième", "Live at Couleurs Café"
        )
        prop_quid_user_tree.graft(
            "konono1.albums.quatrième", "Assume Crash Position"
        )
        prop_quid_user_tree.graft(
            "konono1.albums.cinquième", "Konono Nº1 Meets Batida"
        )
        prop_quid_user_tree.tag_as_array("konono1.albums", True)
        user_tree_json = json.loads(prop_quid_user_tree.json())
        # start the video quids
        vid_quid_1.set("started", True)
        vid_quid_2.set("started", True)
        self.assertTrue(property_quid.get("unsaved_bool_"))
        self.assertTrue(property_quid.get("bool_"))

        # sets the unsaved_bool property of the property_quid to false.
        # this should revert back to true on load since its state won't be saved.
        property_quid.set("unsaved_bool_", False)
        # this one should be saved and restored.
        property_quid.set("bool_", False)

        self.assertTrue(dummy_quid_1.try_connect(vid_quid_1))
        self.assertTrue(dummy_quid_2.try_connect(vid_quid_2))
        # tries to get a raw video shmdata running from usr/lib or usr/local/lib.
        # If it fails, it just tests without raw shmdata
        # This fails on ubuntu 22.04, maybe the gst version is different ?
        raw_shmdata = False
        raw_shmpath = "/tmp/test_video_shmdata"
        gst_commands_to_try = [
            [
                "gst-launch-1.0",
                "--gst-plugin-path=/usr/lib/gstreamer-1.0/",
                "videotestsrc",
                "!",
                "shmdatasink",
                f"socket-path={raw_shmpath}",
            ],
            [
                "gst-launch-1.0",
                "--gst-plugin-path=/usr/local/lib/gstreamer-1.0/",
                "videotestsrc",
                "!",
                "shmdatasink",
                f"socket-path={raw_shmpath}",
            ],
        ]
        process = None
        for i, gst_command in enumerate(gst_commands_to_try):
            process = Popen(gst_command)
            # wait for eventual error code
            sleep(1)
            # asks the process if its still alive. if it is it returns
            # None, else it returns the error code
            result = process.poll()

            # if the result is none, we didn't have an error code, we have a gstreamer process running
            # breaks out of the loop and consider that we have a raw shmdata.
            if result is None:
                raw_shmdata = True
                break
        raw_shmdata = True
        # The meta_follower_quid can be connect to any number of video sources.
        # a new sfid will be generated at each connection.
        self.assertTrue(meta_follower_quid.try_connect(vid_quid_1))
        self.assertTrue(meta_follower_quid.try_connect(vid_quid_2))
        if raw_shmdata:
            # the first follower claw is the meta follower we want to connect to
            self.assertTrue(
                meta_follower_quid.get_follower_claws()[0].connect_raw(
                    raw_shmpath
                )
            )
        # wait for connection
        sleep(1)
        meta_follower_conspecs = json.loads(
            meta_follower_quid.get_connection_specs().json()
        )
        session_name = "_test_session"
        # save the session
        sw.session.save_as(session_name)

        # reads the content to compare with what we expect

        # note : the tuple_ property of the propquid is serialized as a null value.
        # this is a bug, see https://gitlab.com/sat-mtl/tools/switcher/-/issues/190
        session_file_name = (
            "tests/session_raw.json"
            if raw_shmdata
            else "tests/session_no_raw.json"
        )
        expected_session_value = None
        with open(session_file_name) as session_file:
            expected_session_value = json.load(session_file)

        received_session_value = json.loads(sw.session.read(session_name))

        # in the version of gst that ubuntu 22.04, the video quiddities format values
        # have different ids. ignore format key for
        for quid_id in received_session_value["properties"]:
            if received_session_value["properties"][quid_id].get("format"):
                del received_session_value["properties"][quid_id]["format"]

        for quid_id in expected_session_value["properties"]:
            if expected_session_value["properties"][quid_id].get("format"):
                del expected_session_value["properties"][quid_id]["format"]

        # check that the saved session has everything we expect
        print(expected_session_value, received_session_value)
        self.assertTrue(
            compare_session_serializations(
                expected_session_value, received_session_value
            )
        )

        # remove all quids from the session
        self.remove_all_quids()
        # verify everything is gone
        self.assertEqual(list(sw.quiddities), [])
        # loads the session
        sw.session.load(session_name)
        # wait for stuff (maybe useless)
        sleep(1)

        quids = list(sw.quiddities)
        # verify all nicknames and kinds are there in the right order.
        self.assertEqual(
            {quid.nickname() for quid in quids},
            {"dummy1", "dummy2", "vidsrc1", "vidsrc2", "metaquid", "propquid"},
        )
        self.assertEqual(
            {quid.get_kind() for quid in quids},
            {
                "dummysink",
                "dummysink",
                "videotestsrc",
                "videotestsrc",
                "dyn-reader-quid",
                "dummy",
            },
        )
        (
            dummy_quid_1,
            dummy_quid_2,
            vid_quid_1,
            vid_quid_2,
            meta_follower_quid,
            property_quid,
        ) = quids

        # checks that unsaved_bool_ was reverted to its default value of True
        self.assertTrue(property_quid.get("unsaved_bool_"))
        # and that bool_ was restored to False
        self.assertFalse(property_quid.get("bool_"))
        # checks that the user_tree was properly restored
        restored_user_tree = json.loads(property_quid.get_user_tree().json())
        self.assertEqual(restored_user_tree, user_tree_json)
        dummy_quids = filter(
            lambda quid: quid.get_kind() == "dummysink", quids
        )
        writer_quids = filter(
            lambda quid: quid.get_kind() == "videotestsrc", quids
        )

        def get_shmdata_subtree(quid):
            """returns the shmdata subtree of a quiddity or {} if it does not exist"""
            return json.loads(quid.get_info_tree_as_json()).get("shmdata", {})

        # verify that all the quiddities have the appropriate reader or writer shmdata subtrees properties
        # and are therefore connected
        reader_subtrees = [
            get_shmdata_subtree(r_quid).get("reader") for r_quid in dummy_quids
        ]
        writer_subtrees = [
            get_shmdata_subtree(w_quid).get("writer")
            for w_quid in writer_quids
        ]
        self.assertTrue(all(reader_subtrees))
        self.assertTrue(all(writer_subtrees))
        # gets all the first paths of the writer quids
        shmpaths = []
        for tree in writer_subtrees:
            paths = list(tree.keys())
            self.assertTrue(paths)
            shmpaths.append(paths[0] if len(paths) else False)
        # verify that the reader quids are connected to their writer counterpart by
        # looking to see if the corresonding writer shmpaths are present as keys in the
        # shmdata tree of the readers.
        # eg : dummy 1 is connected to vid 1 and dummy 2 to vid 2
        self.assertTrue(
            [
                shmpaths[i] in reader_tree
                for i, reader_tree in enumerate(reader_subtrees)
            ]
        )

        # verify also that the dyn-reader-quid has the same connection specs as before, which means
        # that the connections to it were restored.
        self.assertEqual(
            meta_follower_conspecs,
            json.loads(meta_follower_quid.get_connection_specs().json()),
        )
        # the dyn-reader-quid does not create Follower object for its connected shmdata which means
        # the shmdata part of its infotree will never be populated. This means we cannot precisely test
        # the order of the connections.

        # if raw_shmdata is True, we have a long running process. Terminate it.
        if raw_shmdata:
            process.terminate()
