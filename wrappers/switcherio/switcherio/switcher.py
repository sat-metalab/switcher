#!/usr/bin/env python3

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2.1
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

from argparse import ArgumentParser

import switcherio


def parse_args() -> None:
    parser = ArgumentParser(
        prog="switcher", description="Run a Switcher instance"
    )

    parser.add_argument(
        "--version",
        help="print libswitcher version",
        action="store_true",
    )

    parser.add_argument(
        "--port",
        help="service port (default 8000)",
        type=int,
        nargs="?",
        default=8000,
        const=True,
    )

    parser.add_argument(
        "--debug",
        help="enable Switcher debug logger",
        action="store_true",
    )

    parser.add_argument(
        "--debug-sio",
        help="enable python-socketio debug logger",
        action="store_true",
    )

    return parser.parse_args()


def main():
    args = parse_args()

    if args.version:
        print(
            switcherio.SwitcherIOServer(
                args.debug, args.debug_sio, args.port
            ).switcher_version()
        )
        exit(0)

    switcherio_server = switcherio.SwitcherIOServer(
        args.debug, args.debug_sio, args.port
    )
    switcherio_server.run()


if __name__ == "__main__":
    main()
    print("Exiting __main__.")
