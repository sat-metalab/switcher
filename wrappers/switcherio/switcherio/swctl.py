#!/usr/bin/env python3

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2.1
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

import json
import logging
from argparse import ArgumentParser

import socketio

SWITCHERIO_NAMESPACE = "/switcherio"

# setup logger
logger = logging.getLogger("swctl")
logging.basicConfig(format="%(name)s: %(levelname)s: %(message)s")
logger.setLevel(logging.DEBUG)


def parse_args() -> None:
    parser = ArgumentParser(
        prog="swctl",
        description="Control a `Switcher` instance via Socket.IO",
    )

    # Switcher API
    parser.add_argument(
        "--version",
        action="store_true",
        help="Print version of the connected Switcher instance",
    )
    parser.add_argument(
        "--uri",
        dest="uri",
        action="store",
        default="http://localhost:8000?version=1.0.0",
        help="The server URI to connect to {default: http://localhost:8000}",
    )
    parser.add_argument(
        "-r",
        "--reset-session",
        dest="reset_session",
        action="store_true",
        help="Resets initial session state and deletes all unprotected quiddities.",
    )
    parser.add_argument(
        "-ls",
        "--list-sessions",
        dest="list_sessions",
        action="store_true",
        help="List all session files (JSON files located in ~/.local/state/switcher/sessions/)",
    )
    parser.add_argument(
        "-s",
        "--save-session",
        dest="save_session",
        metavar="SESSION_NAME",
        action="store",
        help="Save state to a session file (JSON file in ~/.local/state/switcher/sessions/SESSION_NAME.json)",
    )
    parser.add_argument(
        "-l",
        "--load-session",
        dest="load_session",
        metavar="SESSION_NAME",
        action="store",
        help="Load state from an existing session file (JSON files located in \
                ~/.local/state/switcher/sessions/SESSION_NAME.json)",
    )

    # Quiddity API
    parser.add_argument(
        "-lk",
        "--list-kinds",
        dest="list_kinds",
        action="store_true",
        help="List available quiddity kinds",
    )
    parser.add_argument(
        "-k",
        "--kind-doc",
        dest="kind_doc",
        nargs="+",
        metavar=("KIND"),
        action="store",
        help="Print documentation for any given kind (unimplemented)",
    )
    parser.add_argument(
        "-K",
        "--kinds-doc",
        dest="kinds_doc",
        action="store_true",
        help="Print available kinds documentation",
    )
    parser.add_argument(
        "-Q",
        "--quiddities-descr",
        dest="quiddities_descr",
        action="store_true",
        help="Print initialized quiddity descriptions (unimplemented)",
    )
    parser.add_argument(
        "-c",
        "--create-quiddity",
        dest="create_quiddity",
        nargs="+",
        metavar=("KIND", "NICKNAME"),
        action="store",
        help="Initialize a new quiddity for any given kind and nickname (optional).",
    )
    parser.add_argument(
        "-qr",
        "--read-quiddity",
        dest="read_quiddity",
        nargs="+",
        metavar="NICKNAME",
        action="store",
        help="Read a quiddity description for any given nickname (unimplemented)",
    )
    parser.add_argument(
        "-d",
        "--delete-quiddity",
        dest="delete_quiddity",
        nargs="+",
        metavar="NICKNAME",
        action="store",
        help="Delete a quiddity instance for any given nickname",
    )
    parser.add_argument(
        "-lq",
        "--list-quiddities",
        dest="list_quiddities",
        action="store_true",
        help="List current quiddity instances",
    )
    subparsers = parser.add_subparsers(title="commands", dest="subparser_name")
    parser_invoke = subparsers.add_parser(
        "invoke",
        help="Invoke method of a quiddity (NICKNAME METHOD_NAME [METHOD_ARGS ...])",
    )
    parser_invoke.add_argument(
        "quiddity_nickname",
        metavar="NICKNAME",
        help="Nickname of quiddity to invoke method",
    )
    parser_invoke.add_argument(
        "method_name",
        metavar="METHOD_NAME",
        help="Name of method to invoke",
    )
    parser_invoke.add_argument(
        "method_args",
        nargs="*",
        metavar="METHOD_ARGS",
        help="variable number of method arguments (optional)",
    )

    # Property API
    parser.add_argument(
        "-sp",
        "--set-prop",
        dest="set_prop",
        nargs=3,
        metavar=("NICKNAME", "PROP_NAME", "PROP_VALUE"),
        action="append",
        help="Set property value for a given quiddity nickname",
    )
    parser.add_argument(
        "-gp",
        "--get-prop",
        dest="get_prop",
        nargs=2,
        metavar=("NICKNAME", "PROP_NAME"),
        action="append",
        help="Get property value for a given quiddity nickname",
    )

    # Connect API
    parser.add_argument(
        "-o",
        "--try-connect",
        dest="try_connect",
        nargs=2,
        metavar=("NICKNAME", "NICKNAME"),
        action="append",
        help="Try to connect two quiddities given their nickname",
    )
    parser.add_argument(
        "-O",
        "--conn-spec",
        dest="conn_spec",
        nargs=1,
        metavar="NICKNAME",
        action="append",
        help="Print current shmdata connection specification for a given quiddity identifier",
    )

    parser.add_argument(
        "-C",
        "--connections",
        action="store_true",
        help="Prints a json representation of the connections this switcher knows about",
    )

    # Extras API
    parser.add_argument(
        "-pt",
        "--print-tree",
        dest="print_tree",
        nargs="*",
        metavar=("NICKNAME", "BRANCH"),
        action="append",
        help="Print information tree given a quiddity identifier and an optional branch path",
    )
    parser.add_argument(
        "-pu",
        "--print-user-data",
        dest="print_user_data",
        nargs="*",
        metavar=("NICKNAME", "BRANCH"),
        action="append",
        help="Print user data tree given a quiddity nickname and an optional branch path (unimplemented)",
    )
    parser.add_argument(
        "-gu",
        "--graft-user-data",
        dest="graft_user_data",
        nargs=3,
        metavar=("NICKNAME", "BRANCH", "ARGS"),
        action="append",
        help="Graft user data tree value (unimplemented)",
    )
    parser.add_argument(
        "-ru",
        "--prune-user-data",
        dest="prune_user_data",
        nargs=2,
        metavar=("NICKNAME", "BRANCH"),
        action="append",
        help="Prune user data tree given a quiddity nickname and a branch path (unimplemented)",
    )
    parser.add_argument(
        "-ta",
        "--tag-as-array-user-data",
        dest="tag_as_array_user_data",
        nargs=2,
        metavar=("NICKNAME", "BRANCH"),
        action="append",
        help="Tag branch as array in the user data tree (unimplemented)",
    )
    parser.add_argument(
        "-p",
        "--shmpath",
        dest="shmpath",
        nargs=2,
        metavar=("NICKNAME", "LABEL"),
        action="append",
        help="Get shmpath of a writer given its label",
    )
    parser.add_argument(
        "-lb",
        "--load-bundles",
        dest="load_bundles",
        nargs=1,
        metavar=("PATH"),
        action="append",
        help="Makes switcher load the bundles described in the file at the given path",
    )

    return parser.parse_args()


def exit_on_error(sio):
    sio.disconnect()
    exit(1)


def exit_on_success(sio):
    sio.disconnect()
    exit(0)


def quid_nickname_to_quid_id(sio, quid_nickname):
    err, quid_id = sio.call(
        "switcher_quiddity_id",
        data=quid_nickname,
        namespace=SWITCHERIO_NAMESPACE,
    )
    if err:
        print(err)
        exit_on_error(sio)
    else:
        if quid_id is None:
            print(f"No quiddity with nickname '{quid_nickname}' found")
            exit_on_error(sio)
        return quid_id


def query(args, sio):
    if args.version:
        _, version = sio.call(
            "switcher_version", namespace=SWITCHERIO_NAMESPACE
        )
        print(version)
    elif args.list_kinds:
        _, res = sio.call("switcher_kinds", namespace=SWITCHERIO_NAMESPACE)
        for kind_name in [kind["kind"] for kind in res["kinds"]]:
            print(kind_name)
    elif args.list_quiddities:
        _, quids = sio.call(
            "switcher_quiddities", namespace=SWITCHERIO_NAMESPACE
        )
        for quid in quids:
            print(f"{quid['nickname']}")
    elif args.kinds_doc:
        _, res = sio.call("switcher_kinds", namespace=SWITCHERIO_NAMESPACE)
        for kind in res["kinds"]:
            print(
                f"{kind['kind']}\n\tname: {kind['name']}\n"
                f"\tdescription: {kind['description']}\n"
                f"\tauthor: {kind['author']}\n\tlicense: {kind['license']}"
            )
    elif args.connections:
        err, connections = sio.call(
            "switcher_get_connections", namespace=SWITCHERIO_NAMESPACE
        )
        print(connections)
    elif args.kind_doc:
        print("option -k / --kind-doc is not implemented yet")
    elif args.quiddities_descr:
        print("option -Q / --quiddities-descr is not implemented yet")
    elif args.reset_session:
        err, res = sio.call("session_reset", namespace=SWITCHERIO_NAMESPACE)
        if err:
            print(res)
            exit_on_error(sio)
        else:
            print(res)
    elif args.list_sessions:
        err, session_names = sio.call(
            "session_list", namespace=SWITCHERIO_NAMESPACE
        )
        if err:
            print(res)
            exit_on_error(sio)
        else:
            for session_name in session_names:
                print(f"{session_name}")
    elif args.save_session:
        session_name = args.save_session
        err, res = sio.call(
            "session_save_as",
            data=session_name,
            namespace=SWITCHERIO_NAMESPACE,
        )
        if err:
            print(res)
            exit_on_error(sio)
        else:
            print(res)
    elif args.load_session:
        session_name = args.load_session
        err, res = sio.call(
            "session_load", data=session_name, namespace=SWITCHERIO_NAMESPACE
        )
        if err:
            print(res)
            exit_on_error(sio)
        else:
            print(res)
    elif args.create_quiddity:
        data = args.create_quiddity
        try:
            try:
                quid_kind, quid_nickname = data[0], data[1]
            except IndexError:
                quid_kind = data[0]
                quid_nickname = None
            err, res = sio.call(
                "quiddity_create",
                data=(quid_kind, quid_nickname),
                namespace=SWITCHERIO_NAMESPACE,
            )
            if err:
                logger.error(err)
                exit_on_error(sio)
            else:
                print(f"{res['nickname']}")
        except Exception as e:
            logger.exception(e)
            exit_on_error(sio)
    elif args.delete_quiddity:
        for quid_nickname in args.delete_quiddity:
            quid_id = quid_nickname_to_quid_id(sio, quid_nickname)
            err, res = sio.call(
                "quiddity_delete", data=quid_id, namespace=SWITCHERIO_NAMESPACE
            )
            if err:
                logger.error(err)
                exit_on_error(sio)
            else:
                print(res)
    elif args.set_prop:
        for data in args.set_prop:
            try:
                quid_nickname, prop_name, prop_value = (
                    data[0],
                    data[1],
                    data[2],
                )
                quid_id = quid_nickname_to_quid_id(sio, quid_nickname)
                err, res = sio.call(
                    "property_set",
                    data=(quid_id, prop_name, prop_value),
                    namespace=SWITCHERIO_NAMESPACE,
                )
                if err:
                    logger.error(err)
                    exit_on_error(sio)
                else:
                    print(res)
            except Exception as e:
                logger.exception(e)
                continue
    elif args.get_prop:
        for data in args.get_prop:
            try:
                quid_nickname, prop_name = data[0], data[1]
                quid_id = quid_nickname_to_quid_id(sio, quid_nickname)
                err, prop_value = sio.call(
                    "property_get",
                    data=(quid_id, prop_name),
                    namespace=SWITCHERIO_NAMESPACE,
                )
                if err:
                    logger.error(err)
                    exit_on_error(sio)
                else:
                    print(prop_value)
            except Exception as e:
                logger.exception(e)
                continue
    elif args.try_connect:
        for data in args.try_connect:
            src_quid_nickname, dst_quid_nickname = data
            src_quid_id = quid_nickname_to_quid_id(sio, src_quid_nickname)
            dst_quid_id = quid_nickname_to_quid_id(sio, dst_quid_nickname)
            err, res = sio.call(
                "quiddity_connect",
                data=(src_quid_id, dst_quid_id),
                namespace=SWITCHERIO_NAMESPACE,
            )
            if err:
                logger.error(err)
                exit_on_error(sio)
            else:
                print(res)
    elif args.conn_spec:
        for data in args.conn_spec:
            try:
                quid_nickname = data[0]
                quid_id = quid_nickname_to_quid_id(sio, quid_nickname)
                err, conn_specs = sio.call(
                    "connection_specs_get",
                    data=(quid_id,),
                    namespace=SWITCHERIO_NAMESPACE,
                )
                if err:
                    logger.error(err)
                    exit_on_error(sio)
                else:
                    info_tree_json = json.dumps(conn_specs, indent=2)
                    print(conn_specs)
            except Exception as e:
                logger.exception(e)
    elif args.read_quiddity:
        print("option -qr / --read-quiddity is not implemented yet")
    elif args.print_tree:
        for data in args.print_tree:
            try:
                quid_nickname, tree_path = data[0], data[1]
                quid_id = quid_nickname_to_quid_id(sio, quid_nickname)
                err, info_tree = sio.call(
                    "info_tree_get",
                    data=(quid_id, tree_path),
                    namespace=SWITCHERIO_NAMESPACE,
                )
                if err:
                    logger.error(err)
                    exit_on_error(sio)
                else:
                    info_tree_json = json.dumps(info_tree, indent=2)
                    print(info_tree_json)
            except Exception as e:
                logger.exception(e)
                continue
    elif args.print_user_data:
        print("option -pu / --print-user-data is not implemented yet")
    elif args.graft_user_data:
        print("option -gu / --graft-user-data is not implemented yet")
    elif args.prune_user_data:
        print("option -ru / --prune-user-data is not implemented yet")
    elif args.tag_as_array_user_data:
        print("option -ta / --tag-as-array-user-data is not implemented yet")
    elif args.shmpath:
        for data in args.shmpath:
            try:
                quid_nickname, wclaw_label = data[0], data[1]
                quid_id = quid_nickname_to_quid_id(sio, quid_nickname)
                err, shmpath = sio.call(
                    "quiddity_shmpath",
                    data=(quid_id, wclaw_label),
                    namespace=SWITCHERIO_NAMESPACE,
                )
                if err:
                    logger.error(err)
                    exit_on_error(sio)
                else:
                    print(shmpath)
            except Exception as e:
                logger.exception(e)
                continue
    elif args.subparser_name == "invoke":
        try:
            quid_nickname = args.quiddity_nickname
            method_name = args.method_name
            method_args = args.method_args
            quid_id = quid_nickname_to_quid_id(sio, quid_nickname)
            err, res = sio.call(
                "quiddity_invoke",
                data=(quid_id, method_name, method_args),
                namespace=SWITCHERIO_NAMESPACE,
            )
            if err:
                logger.error(err)
                exit_on_error(sio)
            else:
                print(res)
        except Exception as e:
            logger.exception(e)
            exit_on_error(sio)
    elif args.load_bundles:
        for data in args.load_bundles:
            try:
                path = data[0]
                bundles = ""
                with open(path) as bundles_file:
                    bundles = bundles_file.read()
                err, info_tree = sio.call(
                    "switcher_bundles",
                    data=(bundles),
                    namespace=SWITCHERIO_NAMESPACE,
                )
                if err:
                    logger.error(err)
                    exit_on_error(sio)
                else:
                    print("succesfully set bundles")
            except Exception as e:
                logger.exception(e)
                continue
    else:
        exit_on_error(sio)

    # every commands have been processed
    exit_on_success(sio)


def main():
    args = parse_args()

    sio = socketio.Client(logger=False, engineio_logger=False)

    try:
        sio.connect(args.uri, namespaces=SWITCHERIO_NAMESPACE)
    except Exception as e:
        logger.error(e)
        logger.debug(
            f"Is the `switcherio` server running at {args.uri.split('?')[0]} ?"
        )
        exit(1)

    query(args, sio)


if __name__ == "__main__":
    main()
    print("Exiting __main__.")
