#!/usr/bin/env python3

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2.1
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

import time

from .base import SocketIOTestCase

# test quiddity data
videotest_quid = {
    "kind": "videotestsrc",
    "nickname": "test",
    "properties": {},
    "user_data": {"testData": True},
}

signal_quid = {"kind": "signal", "nickname": "signal"}


class TreeTestCase(SocketIOTestCase):
    # reverences on the created quiddities
    videotest_id = None
    signal_id = None

    def setUp(self):
        # creation of a videotestsrc quiddity
        data = tuple(videotest_quid.values())
        err, res = self.sio.call(
            "quiddity_create", data=data, namespace=self.namespace
        )
        self.assertIsNone(err)
        self.videotest_id = res["id"]
        # creation of a signal quiddity
        data = tuple(signal_quid.values())
        err, res = self.sio.call(
            "quiddity_create", data=data, namespace=self.namespace
        )
        self.assertIsNone(err)
        self.signal_id = res["id"]

    def tearDown(self):
        err, res = self.sio.call("session_clear", namespace=self.namespace)
        self.videotest_id = None
        self.signal_id = None
        self.rcvd_data = []


class UserTreeTestCase(TreeTestCase):
    """Test case for the `UserTree API`

    Available tests:
      - user_tree_get
      - user_tree_graft
      - user_tree_prune
      - on_user_tree_grafted
      - on_user_tree_pruned
    """

    events = ["user_tree_grafted", "user_tree_pruned"]

    def test_create_quid_with_data(self):
        err, res = self.sio.call(
            "user_tree_get", data=(self.videotest_id), namespace=self.namespace
        )
        self.assertEqual(res["testData"], True)

    def test_set_user_tree(self):
        err, res = self.sio.call(
            "user_tree_graft",
            data=(self.videotest_id, "testData", False),
            namespace=self.namespace,
        )
        self.assertIsNone(err)
        self.assertIsInstance(res, dict)
        self.assertEqual(res["testData"], False)

    def test_get_user_tree(self):
        # add some data in the user tree
        self.sio.call(
            "user_tree_graft",
            data=(self.videotest_id, "testData", True),
            namespace=self.namespace,
        )
        self.sio.call(
            "user_tree_graft",
            data=(self.videotest_id, "testTree.value", 4),
            namespace=self.namespace,
        )

        # get a non empty user tree
        err, res = self.sio.call(
            "user_tree_get", data=(self.videotest_id), namespace=self.namespace
        )
        self.assertIsNone(err)
        self.assertIsInstance(res, dict)
        self.assertEqual(res["testData"], True)

        # query a non empty value
        err, res = self.sio.call(
            "user_tree_get",
            data=(self.videotest_id, "testTree.value"),
            namespace=self.namespace,
        )
        self.assertIsNone(err)
        self.assertIsInstance(res, int)
        self.assertEqual(res, 4)

        # query an empty value
        err, res = self.sio.call(
            "user_tree_get",
            data=(self.videotest_id, "testTree.fail"),
            namespace=self.namespace,
        )
        self.assertIsNone(err)
        self.assertEqual(res, None)

    def test_user_tree_grafted(self):
        err, res = self.sio.call(
            "user_tree_graft",
            data=(self.videotest_id, "testData", False),
            namespace=self.namespace,
        )
        time.sleep(0.1)
        event, grafted_data = self.rcvd_data.pop()
        self.assertEqual(event, "user_tree_grafted")
        id, path, value = grafted_data
        self.assertEqual(id, self.videotest_id)
        self.assertEqual(path, "testData")
        self.assertEqual(value, False)

    def test_user_tree_grafted_on_creation(self):
        videotest_quid2 = dict(videotest_quid)
        videotest_quid2["nickname"] = "test2"
        err, res = self.sio.call(
            "quiddity_create",
            data=tuple(videotest_quid2.values()),
            namespace=self.namespace,
        )
        self.assertIsNone(err)
        time.sleep(0.1)
        event, grafted_data = self.rcvd_data.pop()
        self.assertEqual(event, "user_tree_grafted")
        self.assertEqual(
            grafted_data, (res["id"], ".", videotest_quid["user_data"])
        )

    def test_user_tree_pruned(self):
        self.sio.call(
            "user_tree_graft",
            data=(self.videotest_id, "testData", False),
            namespace=self.namespace,
        )
        err, res = self.sio.call(
            "user_tree_prune",
            data=(self.videotest_id, "testData"),
            namespace=self.namespace,
        )
        self.assertEqual(res, True)
        time.sleep(0.1)
        event, pruned_data = self.rcvd_data.pop()
        self.assertEqual(event, "user_tree_pruned")
        id, path = pruned_data
        self.assertEqual(id, self.videotest_id)
        self.assertEqual(path, "testData")
        err, res = self.sio.call(
            "user_tree_prune",
            data=(self.videotest_id, "testData"),
            namespace=self.namespace,
        )
        self.assertEqual(res, False)


class ConnectionSpecsTestCase(TreeTestCase):
    """Test case for the `ConnectionSpecs API`

    Available tests:
      - connection_specs_get
    """

    def test_get_connection_specs(self):
        err, res = self.sio.call(
            "connection_specs_get",
            data=(self.videotest_id),
            namespace=self.namespace,
        )
        time.sleep(0.1)
        self.assertIsNone(err)
        self.assertIsInstance(res, dict)
        self.assertEqual(res["writer"][0]["can_do"][0], "video/x-raw")


class InfoTreeTestCase(TreeTestCase):
    """Test case for the `InfoTree API`

    Available tests:
      - info_tree_get
      - info_tree_grafted
      - info_tree_pruned
    """

    events = ["info_tree_grafted", "info_tree_pruned"]

    def test_get_info_tree(self):
        # get a non empty user tree
        err, res = self.sio.call(
            "info_tree_get", data=(self.videotest_id), namespace=self.namespace
        )
        self.assertIsNone(err)
        self.assertIsInstance(res, dict)
        self.assertIsInstance(res["property"], list)

        # query a non empty value
        err, res = self.sio.call(
            "info_tree_get",
            data=(self.videotest_id, "property"),
            namespace=self.namespace,
        )
        self.assertIsNone(err)
        self.assertIsInstance(res, list)

        # query an empty value
        err, res = self.sio.call(
            "info_tree_get",
            data=(self.videotest_id, "fail"),
            namespace=self.namespace,
        )
        self.assertIsNone(err)
        self.assertEqual(res, None)

    def test_info_tree_grafted(self):
        err, res = self.sio.call(
            "quiddity_invoke",
            data=(self.signal_id, "do-graft-tree", []),
            namespace=self.namespace,
        )
        time.sleep(0.1)
        (
            event,
            grafted_data,
        ) = self.rcvd_data.pop()  # we should get shmdata stats
        self.assertEqual(event, "info_tree_grafted")
        self.assertIsNotNone(grafted_data)

    def test_info_tree_pruned(self):
        self.sio.call(
            "quiddity_invoke",
            data=(self.signal_id, "do-graft-tree", []),
            namespace=self.namespace,
        )
        err, res = self.sio.call(
            "quiddity_invoke",
            data=(self.signal_id, "do-prune-tree", []),
            namespace=self.namespace,
        )
        time.sleep(0.1)
        event, pruned_data = self.rcvd_data.pop()
        self.assertEqual(event, "info_tree_pruned")
        self.assertIsNotNone(pruned_data)
