#!/usr/bin/env python3

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2.1
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

import os

from .base import SocketIOTestCase

# test quiddity data
videotest_quid = {
    "kind": "videotestsrc",
    "nickname": "test",
    "properties": {},
    "user_data": {"testData": True},
}

signal_quid = {"kind": "signal", "nickname": "signal"}


class SessionTestCase(SocketIOTestCase):
    session_name = "session_test0.json"

    def tearDown(self):
        self.sio.call("session_clear", namespace=self.namespace)
        self.sio.call(
            "session_remove", data=self.session_name, namespace=self.namespace
        )

    def create_quiddity(self, quid_model):
        data = tuple(quid_model.values())
        err, res = self.sio.call(
            "quiddity_create", data=data, namespace=self.namespace
        )
        self.assertIsNone(err)
        self.assertEqual(res["kind"], quid_model["kind"])

    def check_quiddities(self, number_of_quids, quid_nickname=None):
        err, res = self.sio.call(
            "switcher_quiddities", namespace=self.namespace
        )
        self.assertIsNone(err)
        self.assertEqual(len(res), number_of_quids)

        if quid_nickname:
            quid = [q for q in res if q["nickname"] == quid_nickname][0]
            self.assertIsNotNone(quid)

    def save_session(self, session_name):
        err, res = self.sio.call(
            "session_save_as", data=session_name, namespace=self.namespace
        )
        self.assertIsNone(err)
        self.assertIsInstance(res, str)
        self.assertTrue(os.path.exists(res))
        return res

    def test_load_and_remove_a_session(self):
        self.create_quiddity(videotest_quid)
        self.check_quiddities(1)
        self.save_session(self.session_name)
        err, res = self.sio.call("session_reset", namespace=self.namespace)
        self.assertIsNone(err)
        self.check_quiddities(0)
        err, res = self.sio.call(
            "session_load", data=self.session_name, namespace=self.namespace
        )
        self.assertIsNone(err)
        self.check_quiddities(1, videotest_quid["nickname"])
        err, res = self.sio.call("session_list", namespace=self.namespace)
        self.assertIsNone(err)
        self.assertEqual(self.session_name in res, True)
        err, res = self.sio.call(
            "session_remove", data=self.session_name, namespace=self.namespace
        )
        self.assertIsNone(err)
        err, res = self.sio.call("session_list", namespace=self.namespace)
        self.assertIsNone(err)
        self.assertEqual(self.session_name in res, False)

    def test_reset_session(self):
        self.create_quiddity(videotest_quid)
        # creates a signal quid and protects it.
        self.create_quiddity(signal_quid)
        err, res = self.sio.call(
            "switcher_quiddities", namespace=self.namespace
        )
        signal_id = list(
            filter(lambda quid: quid["nickname"] == "signal", res)
        )[0]["id"]
        # checks that we have 2 quids before reset
        self.check_quiddities(2)
        err, res = self.sio.call(
            "quiddities_protect", data=[signal_id], namespace=self.namespace
        )
        err, res = self.sio.call("session_reset", namespace=self.namespace)
        self.assertIsNone(err)
        # and only the signal quid after
        self.check_quiddities(1, signal_quid["nickname"])
        # creates another video quiddity
        self.create_quiddity(videotest_quid)
        # calls protect quiddity without arguments to protect all existing quiddities.
        err, res = self.sio.call(
            "quiddities_protect", namespace=self.namespace
        )
        # see if it kept both quiddities after a call to reset
        err, res = self.sio.call("session_reset", namespace=self.namespace)
        self.check_quiddities(2)
