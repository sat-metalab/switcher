#!/usr/bin/env python3

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2.1
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

import time

from .base import SocketIOTestCase


class QuiddityTestCase(SocketIOTestCase):
    """Test case for the `Quiddity API`

    Available tests:
      - nickname_get
      - nickname_set
      - property_get
      - property_set
      - quiddity_connect
      - quiddity_create
      - quiddity_delete
      - quiddity_list
      - quiddity_invoke
    """

    # registering some generic event callbacks for testing purposes
    events = ["quiddity_created", "quiddity_deleted", "nickname_updated"]

    # test quiddity data
    test_quid = {
        "kind": "dummy",
        "nickname": "test_name",
        "properties": {},
        "user_data": {"testData": True},
    }

    def test_01_quiddity_create(self):
        data = tuple(self.test_quid.values())
        err, res = self.sio.call(
            "quiddity_create", data=data, namespace=self.namespace
        )
        time.sleep(0.1)
        self.assertIsNone(err)
        self.assertIsInstance(res, dict)

    def test_02_property_set(self):
        err, res = self.sio.call(
            "quiddity_create",
            data=("videotestsrc", "vid1"),
            namespace=self.namespace,
        )
        time.sleep(0.1)

        self.assertIsNone(err)
        self.assertIsInstance(res, dict)
        # save the quiddity id since it will be used in the next tests
        global test_src_quiddity_id
        test_src_quiddity_id = res["id"]
        data = (test_src_quiddity_id, "started", True)
        err, res = self.sio.call(
            "property_set", data=data, namespace=self.namespace
        )
        time.sleep(0.1)
        self.assertIsNone(err)
        self.assertTrue(res)

    def test_03_property_get(self):
        err, res = self.sio.call(
            "property_get",
            data=(test_src_quiddity_id, "started"),
            namespace=self.namespace,
        )
        self.assertIsNone(err)
        self.assertTrue(res)

    def test_04_nickname_set(self):
        err, res = self.sio.call(
            "nickname_set",
            data=(test_src_quiddity_id, "video1"),
            namespace=self.namespace,
        )
        time.sleep(0.1)
        self.assertIsNone(err)
        self.assertTrue(res)
        event, updated_data = self.rcvd_data.pop()
        self.assertEqual("nickname_updated", event)
        self.assertIsInstance(updated_data[0], str)
        self.assertEqual(updated_data[1], "video1")

    def test_05_nickname_get(self):
        err, res = self.sio.call(
            "nickname_get", data=test_src_quiddity_id, namespace=self.namespace
        )
        time.sleep(0.1)
        self.assertIsNone(err)
        self.assertEqual(res, "video1")

    def test_06_quiddity_connect(self):
        err, res = self.sio.call(
            "quiddity_create", data=("glfwin", "win"), namespace=self.namespace
        )
        time.sleep(0.1)
        self.assertIsNone(err)
        self.assertIsInstance(res, dict)
        # save the glfwins id.
        global glfwind_quiddity_id
        glfwind_quiddity_id = res["id"]
        err, sfid = self.sio.call(
            "quiddity_connect",
            data=(test_src_quiddity_id, glfwind_quiddity_id),
            namespace=self.namespace,
        )
        self.assertIsNone(err)
        self.assertIsInstance(sfid, int)
        time.sleep(0.1)
        err, res = self.sio.call(
            "quiddity_disconnect",
            data=(glfwind_quiddity_id, sfid),
            namespace=self.namespace,
        )
        self.assertIsNone(err)
        self.assertTrue(res)

    def test_07_method_invokation(self):
        err, res = self.sio.call(
            "quiddity_create",
            data=("methodquid", "mquid"),
            namespace=self.namespace,
        )
        time.sleep(0.1)
        self.assertIsNone(err)
        self.assertIsInstance(res, dict)
        # saves the method quiddity id.
        global method_quiddity_id
        method_quiddity_id = res["id"]
        err, res = self.sio.call(
            "quiddity_invoke",
            data=(method_quiddity_id, "hello", ["Albert Camus"]),
            namespace=self.namespace,
        )
        time.sleep(0.1)
        self.assertIsNone(err)
        self.assertIsInstance(res, str)

    def test_08_quiddity_delete(self):
        err, res = self.sio.call(
            "quiddity_delete",
            data=method_quiddity_id,
            namespace=self.namespace,
        )
        time.sleep(0.1)
        self.assertIsNone(err)
        self.assertTrue(res)
        event, deleted_data = self.rcvd_data.pop()
        self.sio.logger.debug(self.rcvd_data)
        self.assertIsInstance(deleted_data[0], str)
