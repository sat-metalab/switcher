Websockets
======

The `SwitcherIO` package essentially act as a bidirectionnal bridge between [Switcher](https://gitlab.com/sat-mtl/tools/switcher) and the front-end, and makes use of [PyQuid](../../doc/python-scripting.md).

In fact, the goal is to roughly have a 1:1 equivalency between a Websocket route and a [PyQuid](../../doc/python-scripting.md) call.

It uses [python-socketio](https://python-socketio.readthedocs.io/en/latest/) with an [aiohttp](https://docs.aiohttp.org/en/stable/) backend to provide an async web server capable of forwarding requests to Switcher (with the help of the [PyQuid](../../doc/python-scripting.md) module).

```mermaid
sequenceDiagram
    WebClient->>SwitcherIO: Connect
    WebClient->>SwitcherIO: Send `quiddity_create` event
    SwitcherIO->>Switcher: Create `Quiddity` instance
    Switcher->>SwitcherIO: Return new instance identifier
    SwitcherIO->>WebClient: Send `quiddity_created` event
    WebClient->>SwitcherIO: Disconnect
```

# Prerequisites

| Software  | Version | Instructions
|-----------|---------|-------------
| Switcher  | Latest  | See [install instructions](../../doc/INSTALL.md)

# Getting Started

Create a new python virtual environment using the built-in [venv](https://docs.python.org/3.8/library/venv.html) module:

```bash
python3 -m venv .venv
```

We are now able to **activate** the virtual environment by sourcing the `activate script` from the [SwitcherIO]() module:

```bash
source .venv/bin/activate
```

Finally, we can install required dependencies:

```bash
python3 -m pip install --editable .
```

# Running the server

Whenever your virtual environment is **active**, starting the **server** is as easy as:

```bash
python3 -m switcherio.switcher --port 8000 --debug --debug-sio
```

Note that once done, we can **deactivate** the virtual environment by issuing the following function sourced by the environment:

```bash
deactivate
```

# Debugging `switcherio`

In order to debug the `switcher` instance from `switcherio`, you can use gdb, pdb, or both:

```bash
gdb --args python3 -m switcherio.switcher --port 8000 --debug --debug-sio
python3 -m pdb -m switcherio.switcher --port 8000 --debug --debug-sio

# if you feel adventurous...
gdb --args python3 -m pdb -m switcherio.switcher --port 8000 --debug --debug-sio
```

# Testing `switcherio`

From the package directory `wrappers/python-switcherio`, you can run any test cases by issuing:

```bash
python3 -m unittest tests/test_name
```

To run the full test suite, use:

```bash
python3 -m unittest
```

# Installing `switcherio`

On Ubuntu 22.04 and Debian 11 or below :

```bash
sudo apt install python3-pip
python3 -m pip install --user pipx
python3 -m pipx ensurepath
```

On Ubuntu 23.04 and Debian 12 or above :

```bash
sudo apt install python3-pip
sudo apt install pipx
```

```bash
pipx install .
pipx install --force .
pipx upgrade .
```

Run switcher launcher :

```bash
switcher
```

```
$ switcher --help
usage: switcher [-h] [--version] [--port [PORT]] [--debug] [--debug-sio]

Run a Switcher instance

options:
  -h, --help     show this help message and exit
  --version      print version
  --port [PORT]  service port (default 8000)
  --debug        enable Switcher debug logger
  --debug-sio    enable python-socketio debug logger
```
