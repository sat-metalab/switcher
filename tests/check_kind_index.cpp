/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#undef NDEBUG  // get assert in release mode

#include <gst/gst.h>

#include <iostream>
#include <set>
#include <string>
#include <vector>

#include "switcher/quiddity/basic-test.hpp"
#include "switcher/switcher.hpp"

using namespace switcher;

int main() {
  bool success = true;
  {
    Switcher::ptr manager = Switcher::make_switcher("check_kind_index", true);

    // create explicit nickname dummy1
    auto dummy1 = manager->quiddities->create("dummy", "dummy1", nullptr).get();
    assert(dummy1);
    std::string dummy1_nickname = dummy1->get_nickname();
    manager->sw_debug("dummy1 nickname is {}", dummy1_nickname);
    assert(dummy1_nickname == "dummy1");

    // create unnamed, should get generated nickname dummy2
    auto dummy2 = manager->quiddities->create("dummy", "dummy2", nullptr).get();
    assert(dummy2);
    std::string dummy2_nickname = dummy2->get_nickname();
    manager->sw_debug("dummy2 nickname is {}", dummy2_nickname);
    assert(dummy2_nickname == "dummy2");

    // create explicit nickname dummy4, manually skipping dummy3 for now
    auto dummy4 = manager->quiddities->create("dummy", "dummy4", nullptr).get();
    assert(dummy4);
    std::string dummy4_nickname = dummy4->get_nickname();
    manager->sw_debug("dummy4 nickname is {}", dummy4_nickname);
    assert(dummy4_nickname == "dummy4");

    // create unnamed, should get generated nickname dummy3, as index 3 is free
    auto dummy3 = manager->quiddities->create("dummy", "", nullptr).get();
    assert(dummy3);
    std::string dummy3_nickname = dummy3->get_nickname();
    manager->sw_debug("dummy3 nickname is {}", dummy3_nickname);
    assert(dummy3_nickname == "dummy3");

    // create unnamed, should get generated nickname dummy5, as index 4 is
    // already taken
    auto dummy5 = manager->quiddities->create("dummy", "", nullptr).get();
    assert(dummy5);
    std::string dummy5_nickname = dummy5->get_nickname();
    manager->sw_debug("dummy5 nickname is {}", dummy5_nickname);
    assert(dummy5_nickname == "dummy5");

    if (success) {
      manager->sw_debug("check_kind_index successfully finished");
    }
  }

  gst_deinit();
  if (success) {
    return 0;
  } else {
    return 1;
  }
}
