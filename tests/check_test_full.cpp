/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#undef NDEBUG  // get assert in release mode

#include <gst/gst.h>

#include <iostream>
#include <set>
#include <string>
#include <vector>

#include "switcher/quiddity/basic-test.hpp"
#include "switcher/switcher.hpp"

using namespace switcher;

int main() {
  std::set<std::string> tests_to_fake_if_create_fails;
  tests_to_fake_if_create_fails.insert("v4l2src");
  tests_to_fake_if_create_fails.insert("ltcsource");
  tests_to_fake_if_create_fails.insert("ltctojack");
  tests_to_fake_if_create_fails.insert("pulsesink");
  tests_to_fake_if_create_fails.insert("pulsesrc");

  bool success = true;
  {
    Switcher::ptr manager = Switcher::make_switcher("test_full", true);
    for (auto& it : manager->factory<MPtr(&quiddity::Factory::get_kinds)>()) {
      manager->sw_debug("Starting the full test of quiddity {}", it);
      // if the kind is in the set of kinds to fake when the creation fails, continue
      // as if the tests were passing. This allows us to keep the CI passing even
      // if we are missing some ressources to test a quiddity.
      if (tests_to_fake_if_create_fails.count(it) && !quiddity::test::create(manager, it)) {
        manager->sw_warning("Could not create quiddity of kind {}, continuing the tests.", it);
        continue;  // failure to create probably means some missing resource.
      }
      if (!quiddity::test::full(manager, it)) {
        manager->sw_error("Full test of quiddity {} failed", it);
        success = false;
        // when one test fails, don't do all the rest of the tests.
        break;
      }
      manager->sw_debug("Full test of quiddity {} succeeded", it);
    }
    if (success) {
      manager->sw_debug("check_test_full finished");
    }
  }

  gst_deinit();
  if (success) {
    return 0;
  } else {
    return 1;
  }
}
