/*
 * This file is part of switcher.
 *
 * switcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * switcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with switcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG  // get assert in release mode
#include <gst/gst.h>
#include <unistd.h>

#include <cassert>
#include <vector>

#include "switcher/quiddity/basic-test.hpp"
#include "switcher/switcher.hpp"

using namespace switcher;

/**
 * Instanciates a sink quiddity, a video source quiddity and an encoder quiddity.

 Connects the video to the encoder and the encoder to the sink and tests the propagation of
 sfids all along that path.

 * \param manager Switcher instance used for the test
 * \param sink_kind kind of the sink
 * \param video_source_kind kind of the video source
 * \param encoder_kind kind of the encoder
 * \return true if it succeeds, false if it doesn't
 */
bool test_encoder_connections(Switcher::ptr manager,
                              std::string sink_kind,
                              std::string video_source_kind,
                              std::string encoder_kind) {
  // instanciate the sink bundles
  auto dummy_qrox = manager->quiddities->create(sink_kind, "dummy", nullptr);
  assert(dummy_qrox);

  // instanciate the encoder bundle
  auto encoder_qrox = manager->quiddities->create(encoder_kind, "encoder", nullptr);
  assert(encoder_qrox);

  // instanciate a video-test-input bundle
  auto video_src_qrox = manager->quiddities->create(video_source_kind, "vidsrc", nullptr);
  assert(video_src_qrox);

  // starts the video source
  assert(
      video_src_qrox.get()->prop<MPtr(&quiddity::property::PBag::set_str_str)>("started", "true"));

  assert(
      encoder_qrox.get()->claw<MPtr(&quiddity::claw::Claw::try_connect)>(video_src_qrox.get_id()));
  // sleep until the connection is really made. There is a way to do this with signals
  // but when I tried it it segfaulted a lot.
  sleep(2);
  assert(dummy_qrox.get()->claw<MPtr(&quiddity::claw::Claw::try_connect)>(encoder_qrox.get_id()));
  sleep(2);

  // tests the sfid for the encoder quiddity
  auto vid_src_swid =
      video_src_qrox.get()
          ->claw<MPtr(&quiddity::claw::Claw::get_compatible_swids)>(::shmdata::Type("video/x-raw"))
          .at(0);
  // find the shmpath associated with the swid
  std::string vid_writer_shmpath =
      video_src_qrox.get()->claw<MPtr(&quiddity::claw::Claw::get_writer_shmpath)>(vid_src_swid);

  // get the grafted sfid from the shmdata part of the dummy sink infotree.
  quiddity::claw::sfid_t sfid_from_encoder_tree =
      encoder_qrox.get()->tree<MPtr(&InfoTree::get_copy)>()->branch_get_value(
          ".shmdata.reader." + vid_writer_shmpath + ".sfid");
  // gets the sfid stored in the follower_connections_ map in the encoder_qrox's claw.
  auto sfid_from_encoder_claw =
      encoder_qrox.get()->claw<MPtr(&quiddity::claw::Claw::get_follower_sfid)>(vid_writer_shmpath);

  // they should be the same.
  assert(sfid_from_encoder_tree == sfid_from_encoder_claw);
  // test the sfid for the bundle quiddity

  auto encoder_src_swid = 1;
  // find the shmpath associated with the swid
  std::string encoder_writer_shmpath =
      encoder_qrox.get()->claw<MPtr(&quiddity::claw::Claw::get_writer_shmpath)>(encoder_src_swid);

  // get the grafted sfid from the shmdata part of the dummy sink infotree.
  quiddity::claw::sfid_t sfid_from_dummy_tree =
      dummy_qrox.get()->tree<MPtr(&InfoTree::get_copy)>()->branch_get_value(
          ".shmdata.reader." + encoder_writer_shmpath + ".sfid");
  // gets the sfid stored in the follower_connections_ map in the dummy_qrox's claw.
  auto sfid_from_dummy_claw =
      dummy_qrox.get()->claw<MPtr(&quiddity::claw::Claw::get_follower_sfid)>(
          encoder_writer_shmpath);

  // they should be the same.
  assert(sfid_from_dummy_tree == sfid_from_dummy_claw);

  // cleanup quiddities
  manager->quids<MPtr(&quiddity::Container::remove)>(dummy_qrox.get_id());
  manager->quids<MPtr(&quiddity::Container::remove)>(encoder_qrox.get_id());
  manager->quids<MPtr(&quiddity::Container::remove)>(video_src_qrox.get_id());
  return true;
}

int main() {
  // making the switcher
  Switcher::ptr manager = Switcher::make_switcher("check_bundle_test");

  // loading configuration with bundle
  assert(manager->conf<MPtr(&Configuration::from_file)>("./check_bundle.config"));

  manager->sw_debug("software encoder\n");
  assert(test_encoder_connections(manager, "dummysink", "videotestsrc", "videnc"));

  manager->sw_debug("software encoder bundle\n");
  assert(test_encoder_connections(manager, "dummy-sink-bundle", "video-test-input", "h264Encoder"));

  if (quiddity::test::create(manager, "nvencEncoder")) {
    manager->sw_debug("nvenc encoder bundle\n");
    assert(
        test_encoder_connections(manager, "dummy-sink-bundle", "video-test-input", "nvencEncoder"));
  }
  gst_deinit();
  return 0;
}
