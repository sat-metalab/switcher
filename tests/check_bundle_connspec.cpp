/*
 * This file is part of switcher.
 *
 * switcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * switcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with switcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG  // get assert in release mode

#include <gst/gst.h>

#include <atomic>
#include <cassert>
#include <condition_variable>

#include "switcher/infotree/information-tree.hpp"  // remove me
#include "switcher/quiddity/basic-test.hpp"
#include "switcher/switcher.hpp"

using namespace switcher;

int main() {
  {
    // making the switcher
    Switcher::ptr manager = Switcher::make_switcher("bundle", true);

    // loading configuration with bundle
    assert(manager->conf<MPtr(&Configuration::from_file)>("./check_bundle_connspec.json"));

    // creating pure version of delay
    auto src_sink_qrox = manager->quiddities->create("shmdelay", "shmdelay", nullptr);
    assert(src_sink_qrox);

    auto src_sink_quid = src_sink_qrox.get();
    assert(src_sink_quid);

    // print connection specs json for pure quiddity
    std::cout << "connection specs for pure version of shmdelay:" << std::endl;
    std::cout << src_sink_quid->conspec<MPtr(&InfoTree::get_copy)>()->serialize_json() << '\n';

    // creating bundled version of delay
    auto bundled_src_sink_qrox =
        manager->quiddities->create("shmdelay-bundle", "shmdelay-bundle", nullptr);
    assert(bundled_src_sink_qrox);

    auto bundled_src_sink_quid = bundled_src_sink_qrox.get();
    assert(bundled_src_sink_quid);

    // print connection specs json for bundled quiddity
    std::cout << "connection specs for bundled version of shmdelay:" << std::endl;
    std::cout << bundled_src_sink_quid->conspec<MPtr(&InfoTree::get_copy)>()->serialize_json()
              << '\n';

    // compare sfids between pure and bundled delay quiddity
    auto src_sink_quid_shm_sfid = src_sink_quid->claw<MPtr(&quiddity::claw::Claw::get_sfid)>("shm");
    assert(src_sink_quid_shm_sfid);

    auto bundled_src_sink_quid_shm_sfid =
        bundled_src_sink_quid->claw<MPtr(&quiddity::claw::Claw::get_sfid)>("Delay/shm");
    assert(bundled_src_sink_quid_shm_sfid);

    assert(src_sink_quid_shm_sfid == bundled_src_sink_quid_shm_sfid);

    auto src_sink_quid_ltc_diff_sfid =
        src_sink_quid->claw<MPtr(&quiddity::claw::Claw::get_sfid)>("ltf-diff");
    assert(src_sink_quid_ltc_diff_sfid);

    auto bundled_src_sink_quid_ltc_diff_sfid =
        bundled_src_sink_quid->claw<MPtr(&quiddity::claw::Claw::get_sfid)>("Delay/ltf-diff");
    assert(bundled_src_sink_quid_ltc_diff_sfid);

    assert(src_sink_quid_ltc_diff_sfid == bundled_src_sink_quid_ltc_diff_sfid);

    // compare swids between pure and bundled delay quiddity
    auto src_sink_quid_delayed_shm_swid =
        src_sink_quid->claw<MPtr(&quiddity::claw::Claw::get_swid)>("delayed-shm");
    assert(src_sink_quid_delayed_shm_swid);

    auto bundled_src_sink_quid_delayed_shm_swid =
        bundled_src_sink_quid->claw<MPtr(&quiddity::claw::Claw::get_swid)>("Delay/delayed-shm");
    assert(bundled_src_sink_quid_delayed_shm_swid);

    assert(src_sink_quid_delayed_shm_swid == bundled_src_sink_quid_delayed_shm_swid);

    // creating pure version of executor
    auto executor_qrox = manager->quiddities->create("executor", "executor", nullptr);
    assert(executor_qrox);

    auto executor_quid = executor_qrox.get();
    assert(executor_quid);

    // print connection specs json for pure executor
    std::cout << "connection specs for pure version of executor:" << std::endl;
    std::cout << executor_quid->conspec<MPtr(&InfoTree::get_copy)>()->serialize_json() << '\n';

    // creating bundled version of executor
    auto bundled_executor_qrox =
        manager->quiddities->create("executor-bundle", "executor-bundle", nullptr);
    assert(bundled_executor_qrox);

    auto bundled_executor_quid = bundled_executor_qrox.get();
    assert(bundled_executor_quid);

    // print connection specs json for bundled executor
    std::cout << "connection specs for bundled version of shmdelay:" << std::endl;
    std::cout << bundled_executor_quid->conspec<MPtr(&InfoTree::get_copy)>()->serialize_json()
              << '\n';

    // compare swids between pure and bundled executor quiddity
    auto executor_quid_custom_swid =
        executor_quid->claw<MPtr(&quiddity::claw::Claw::get_swid)>("custom");
    assert(executor_quid_custom_swid);

    auto bundled_executor_quid_custom_swid =
        bundled_executor_quid->claw<MPtr(&quiddity::claw::Claw::get_swid)>("Executor/custom");
    assert(bundled_executor_quid_custom_swid);

    assert(executor_quid_custom_swid == bundled_executor_quid_custom_swid);

    auto executor_quid_audio_swid =
        executor_quid->claw<MPtr(&quiddity::claw::Claw::get_swid)>("audio");
    assert(executor_quid_audio_swid);

    auto bundled_executor_quid_audio_swid =
        bundled_executor_quid->claw<MPtr(&quiddity::claw::Claw::get_swid)>("Executor/audio");
    assert(bundled_executor_quid_audio_swid);

    assert(executor_quid_audio_swid == bundled_executor_quid_audio_swid);

    auto executor_quid_video_swid =
        executor_quid->claw<MPtr(&quiddity::claw::Claw::get_swid)>("video");
    assert(executor_quid_video_swid);

    auto bundled_executor_quid_video_swid =
        bundled_executor_quid->claw<MPtr(&quiddity::claw::Claw::get_swid)>("Executor/video");
    assert(bundled_executor_quid_video_swid);

    assert(executor_quid_video_swid == bundled_executor_quid_video_swid);
  }
  gst_deinit();
  return 0;
}
