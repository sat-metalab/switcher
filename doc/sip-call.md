Make a multichannel call with the sip quiddity
=======

## Requirements

You need two SIP credentials in order to connect to the SIP/STUN/TURN servers:
* a SIP user and password, for instance receiver@mondomaine.com and mypassword
* a SIP server address, for instance sip.mondomaine.com
* a STUN server address, for instance stun.mondomaine.com
* a TURN server address, for instance turn.mondomaine.com
* a STUN/TURN user and password, for instance receiver@mondomaine.com and mypassword

In the following example, the callee SIP user will be sender@mondomaine.com.

Note that you can install your own server following the documentation here:
https://gitlab.com/sat-mtl/tools/scenic/sip-server

## Prepare the receiver

In a terminal run switcher.io:
```
wrappers/switcherio/server.py
```

In another terminal, prepare you sip communication:
```
swctl --create-quiddity sip sip
swctl --set-prop sip port 15060 # set local port to bind for sip communications
swctl invoke sip register receiver@mondomaine.com:5060 mypassword
swctl invoke sip set_stun_turn stun.mondomaine.com:3478 turn.mondomaine.com:3478 receiver mypassword

swctl --set-prop sip mode "authorized contacts"
swctl invoke sip add_buddy sender@mondomaine.com
swctl invoke sip authorize sender@mondomaine.com true
swctl --print-tree sip buddies.0
```

## Prepare the sender
In a terminal run switcher:
```
WS_PORT=8001 wrappers/switcherio/server.py

swctl --uri http://localhost:8001 --create-quiddity sip sip
swctl --uri http://localhost:8001 --set-prop sip port 5061
swctl --uri http://localhost:8001 invoke sip register sender@mondomaine.com mypassword
swctl --uri http://localhost:8001 invoke sip set_stun_turn stun.mondomaine.com turn.mondomaine.com sender mypassword

swctl --uri http://localhost:8001 invoke sip add_buddy sender@mondomaine.com
swctl --uri http://localhost:8001 --print-tree sip buddies.0
```

Prepare two audio streams to send
```
swctl --uri http://localhost:8001 --create-quiddity audiotestsrc aud1
swctl --uri http://localhost:8001 --set-prop aud1 wave 0
swctl --uri http://localhost:8001 --set-prop aud1 started true
swctl --uri http://localhost:8001 --create-quiddity audiotestsrc aud2
swctl --uri http://localhost:8001 --set-prop aud2 wave 1
swctl --uri http://localhost:8001 --set-prop aud2 started true
```

Prepare the call, i.e., attach several audio Shmdata to the callee
```
swctl --uri http://localhost:8001 invoke sip attach_shmdata_to_contact $(swctl --uri http://localhost:8001 --shmpath aud1 audio) receiver@mondomaine.com true
swctl --uri http://localhost:8001 invoke sip attach_shmdata_to_contact $(swctl --uri http://localhost:8001 --shmpath aud2 audio) receiver@mondomaine.com true
swctl --uri http://localhost:8001 --print-tree sip buddies.0
```

Then make the caller call the callee:
```
swctl --uri http://localhost:8001 invoke sip send receiver@mondomaine.com
```
If you see from logs the call has been refused, you need to authorize your buddy to send you streams (authorize method of the sip quiddity).

## Check the call is receiving
Ask the callee switcher
```
swctl --print-tree sip buddies # "recv_status" should be "receiving"
swctl --list-quiddities
swctl --print-tree "aud1 ()" .shmdata.writer.
swctl --print-tree "aud2 ()" .shmdata.writer.
```
The receiver has created one quiddity per received stream. Here `switcher_switcher-ws_05979f97-b7ea-4e35-a66a-c73c45bceade_6` and `switcher_switcher-ws_05979f97-b7ea-4e35-a66a-c73c45bceade_5`.

Check data are received with sdflow
```
sdflow /run/user/1000/switcher_switcher-ws_05979f97-b7ea-4e35-a66a-c73c45bceade_6
```

## Warning

If you want to transmit video, you need to compress it, for instance with videnc quiddity.
