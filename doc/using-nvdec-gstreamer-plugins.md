# Using the nvdec GStreamer plugin

## Installing the GStreamer plugins
Starting with GStreamer 1.22, nvh264dec and nvcudah264enc are part of the `gst-plugins-bad` collection.

## Using nvh264dec as the default H264 decoder in Switcher
Switcher uses a GStreamer [__decodebin__](https://gstreamer.freedesktop.org/documentation/application-development/highlevel/playback-components.html) element in order to decode video streams. Decodebins are handy because they automatically identify, create and connect all the necessary plugins for decoding the supplied stream.

However, by default, a decodebin will always use a CPU decoder when decoding H264 streams. In order to use the nvdec plugin instead, we must assign it a higher priority (or rank) than the CPU decoder. Decodebins always uses the decoder with the highest rank.

This can be easily done in the `switcher.json` file, by adding the following entry at the root of the configuration:

```json
"gstreamer" : {
    "primary_priority" : {
      "nvh264dec": 10
    }
 }
```

Inside `primary _priority`, simply specify a GStreamer plugin (in our case, __nvh264dec__), and assign it a priority (integer only). On Switcher's startup, any specified plugin will be assigned the `PRIMARY` rank + the specified number in `switcher.json`. In our case, nvh264dec will have a rank of `PRIMARY`+10, which is higher than the other decoders (they often have a rank of `PRIMARY` only).

More info on the inner workings of this logic is available [here](https://gstreamer.freedesktop.org/documentation/tutorials/playback/hardware-accelerated-video-decoding.html).
