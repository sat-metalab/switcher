# Switcher.IO CLI tutorial

Switcher can be scripted and controlled using CLI, either locally or from a remote machine, using the `swctl` utility all Switcher operations are fully supported, including creating/deleting quiddities, setting/getting properties and connecting/disconnecting quiddities.

## Install `swctl` command CLI tool on your workstation

If you installed Switcher, `swctl` command is already installed and should be available in system path.

You can use pipx to install `swctl` using Git URL. This takes a bit of time, since it caches the whole Git repository.

```
sudo apt install git python3-pip
python3 -m pip install --user pipx
python3 -m pipx ensurepath
pipx install git+https://gitlab.com/sat-mtl/tools/switcher.git@develop#subdirectory=wrappers/switcherio
```

## Start a Switcher.IO server instance

To use `swctl`, you must first start a Switcher.IO server. `swctl` cannot create Switcher instances,it can only attach to existing ones.

```
wrappers/switcherio/server.py --debug
```

Once the server, you can control it by running `swctl` commands on locally.

## Test connection to Switcher.IO server

Ask service which version of server is runnning.

```
swctl --version
```

It should return server version string.

```
$ swctl --version
3.5.0
```

## Control a remote Switcher instance

To control a remote Switcher instance using `swctl`, you can prefix every command with the --uri flag to specify the URI of the remote Switcher instance.

```
swctl --uri=http://192.168.1.100:8000 --list-quiddities
```

You can set an environment variable to point to the remote URI to avoid typing out the full URI on each command. For example.

```
SW_URI="http://192.168.1.100:8000"
swctl --uri=$SW_URI --version
```

## Create a `jacksrc` quiddity

```
swctl --create-quiddity jacksrc <optional-custom-nickname>
```

`--create-quiddity` command returns newly created quiddity's nickname. If you don't provide a nickname, a nickname will be generated.

```
$ swctl --create-quiddity jacksrc
jacksrc1
```

## List current quiddities

```
swctl --list-quiddities
```

`--list-quiddities` command returns a list containing each quiddity's nickname, one quiddity per line.

```
$ swctl --list-quiddities
jacksrc1
```

## Set `started` property to `true`

```
swctl --set-prop jacksrc1 started true
```

`--set-prop` command returns "True" if property was successfully set.

```
$ swctl --set-prop jacksrc1 started true
True
```

## Create and start a `jacksrc` quiddity

We'll use a bash one-liner to create quiddity, capture its nickname (using awk to extract nickname after comma), then set `started` property to `true`.

```
swctl --set-prop $(swctl --create-quiddity jacksrc) started true
```

Command returns "True" if creation and starting succeed.

```
$ swctl --set-prop $(swctl --create-quiddity jacksrc) started true
True
```

## Create and start 8 `jacksrc` quiddity instances.

Using last create-and-start one-liner, we'll wrap it in a bash loop to create 8 instances of the quiddity.

```
for((i=1;i<=8;i+=1)); do swctl --set-prop $(swctl --create-quiddity jacksrc) started true; done
```

Command returns "True" for each successfully started quiddity.

```
$ for((i=1;i<=8;i+=1)); do swctl --set-prop $(swctl --create-quiddity jacksrc) started true; done
True
True
True
True
True
True
True
True
```

## Save current session.

Save a running session to a JSON file located ~/.local/state/switcher/sessions/SESSION_NAME.json.

```
swctl --save-session my-session-1
```

Command returns JSON file absolute path.

```
$ swctl --save-session my-session-1
/home/user/.local/state/switcher/sessions/my-session-1.json
```

## Reset session

Reset session deletes all unprotected quiddities.

```
swctl --reset-session
```

Command returns "True" if session was re-initialized.

```
$ swctl --reset-session
True
```

## List saved sessions

List existing saved sessions.

```
swctl --list-sessions
```

Command returns a list of all available saved session files.

```
$ swctl --list-session
my-session-1.json
test3.json
test4.json
test2.json
test1.json
test_session.json
```

## Load a saved session

Load a saved session by passing its name (no need to pass any path or add .json extension). If session name contains spaces, add quotes around (i.e. "My session name with spaces").

```
swctl --load-session my-session-1
```

Command return "True" if session was sucessfully restored.

```
$ tools/swctl --load-session my-session-1
True
```

## Connect quiddities

Create and start a `videotestsrc` video source.

```
swctl --create-quiddity videotestsrc videotestsrc1
swctl --set-prop videotestsrc1 started
```

Create a `glfwin` video monitor output and connect video test source to it.

```
swctl --create-quiddity glfwin glfwin1
swctl --try-connect videotestsrc1 glfwin1
```

## Print a quiddity InfoTree

To debug a quiddity status, you can print its complete InfoTree as a JSON structure.

```
swctl --create-quiddity audiotestsrc
swctl --print-tree audiotestsrc1 .
```

Command prints out InfoTree JSON.

```
$ tools/swctl -c audiotestsrc
audiotestsrc1
$ tools/swctl --print-tree audiotestsrc1 .
{
  "signal": [
    {
      "id": "on-method-added",
      "name": "on-method-added",
      "description": "A method has been installed"
    },
    {
      "id": "on-method-removed",
      "name": "on-method-removed",
      "description": "A method has been uninstalled"
    },
    {
      "id": "on-tree-grafted",
      "name": "on-tree-grafted",
      "description": "On Tree Grafted"
    },
    {
      "id": "on-tree-pruned",
      "name": "on-tree-pruned",
      "description": "A tree has been pruned from the quiddity tree"
    },
    {
      "id": "on-user-data-grafted",
      "name": "on-user-data-grafted",
      "description": "A tree has been grafted to the quiddity's user data tree"
    },
    {
      "id": "on-user-data-pruned",
      "name": "on-user-data-pruned",
      "description": "A branch has been pruned from the quiddity's user data tree"
    },
    {
      "id": "on-nicknamed",
      "name": "on-nicknamed",
      "description": "A nickname has been given to the quiddity"
    },
    {
      "id": "on-connection-spec-added",
      "name": "on-connection-spec-added",
      "description": "New specification has been added to the connection specification"
    },
    {
      "id": "on-connection-spec-removed",
      "name": "on-connection-spec-removed",
      "description": "Specification has been removed from the connection specification"
    }
  ],
  "property": [
    {
      "id": "started",
      "label": "Started",
      "description": "Start/stop the processing",
      "type": "bool",
      "writable": true,
      "value": false,
      "prop_id": 1,
      "order": 20,
      "parent": "",
      "enabled": true
    },
    {
      "id": "sample_rate",
      "label": "Sample rate",
      "description": "List of supported sample rates",
      "type": "selection",
      "writable": true,
      "value": "2",
      "values": [
        {
          "id": 0,
          "label": "22050"
        },
        {
          "id": 1,
          "label": "32000"
        },
        {
          "id": 2,
          "label": "44100"
        },
        {
          "id": 3,
          "label": "48000"
        },
        {
          "id": 4,
          "label": "88200"
        },
        {
          "id": 5,
          "label": "96000"
        },
        {
          "id": 6,
          "label": "192000"
        }
      ],
      "prop_id": 2,
      "order": 40,
      "parent": "",
      "enabled": true
    },
    {
      "id": "frequency",
      "label": "Frequency",
      "description": "Set sound frequency",
      "type": "double",
      "writable": true,
      "value": 440.0,
      "min": 1.0,
      "max": 20000.0,
      "prop_id": 3,
      "order": 60,
      "parent": "",
      "enabled": true
    },
    {
      "id": "volume",
      "label": "Volume",
      "description": "Set sound volume",
      "type": "float",
      "writable": true,
      "value": 0.5,
      "min": 0.0,
      "max": 1.0,
      "prop_id": 4,
      "order": 80,
      "parent": "",
      "enabled": true
    },
    {
      "id": "channels",
      "label": "Number of channels",
      "description": "Set number of channels",
      "type": "int",
      "writable": true,
      "value": 1,
      "min": 1,
      "max": 128,
      "prop_id": 5,
      "order": 100,
      "parent": "",
      "enabled": true
    },
    {
      "id": "format",
      "label": "Format",
      "description": "List of supported sound formats",
      "type": "selection",
      "writable": true,
      "value": "0",
      "values": [
        {
          "id": 0,
          "label": "S16LE"
        },
        {
          "id": 1,
          "label": "S16BE"
        },
        {
          "id": 2,
          "label": "U16LE"
        },
        {
          "id": 3,
          "label": "U16BE"
        },
        {
          "id": 4,
          "label": "S24_32LE"
        },
        {
          "id": 5,
          "label": "S24_32BE"
        },
        {
          "id": 6,
          "label": "U24_32LE"
        },
        {
          "id": 7,
          "label": "U24_32BE"
        },
        {
          "id": 8,
          "label": "S32LE"
        },
        {
          "id": 9,
          "label": "S32BE"
        },
        {
          "id": 10,
          "label": "U32LE"
        },
        {
          "id": 11,
          "label": "U32BE"
        },
        {
          "id": 12,
          "label": "S24LE"
        },
        {
          "id": 13,
          "label": "S24BE"
        },
        {
          "id": 14,
          "label": "U24LE"
        },
        {
          "id": 15,
          "label": "U24BE"
        },
        {
          "id": 16,
          "label": "S20LE"
        },
        {
          "id": 17,
          "label": "S20BE"
        },
        {
          "id": 18,
          "label": "U20LE"
        },
        {
          "id": 19,
          "label": "U20BE"
        },
        {
          "id": 20,
          "label": "S18LE"
        },
        {
          "id": 21,
          "label": "S18BE"
        },
        {
          "id": 22,
          "label": "U18LE"
        },
        {
          "id": 23,
          "label": "U18BE"
        },
        {
          "id": 24,
          "label": "F32LE"
        },
        {
          "id": 25,
          "label": "F32BE"
        },
        {
          "id": 26,
          "label": "F64LE"
        },
        {
          "id": 27,
          "label": "F64BE"
        },
        {
          "id": 28,
          "label": "S8"
        },
        {
          "id": 29,
          "label": "U8"
        }
      ],
      "prop_id": 6,
      "order": 120,
      "parent": "",
      "enabled": true
    },
    {
      "id": "wave",
      "label": "wave",
      "description": "Oscillator waveform",
      "type": "selection",
      "writable": true,
      "value": "0",
      "values": [
        {
          "id": 0,
          "label": "Sine"
        },
        {
          "id": 1,
          "label": "Square"
        },
        {
          "id": 2,
          "label": "Saw"
        },
        {
          "id": 3,
          "label": "Triangle"
        },
        {
          "id": 4,
          "label": "Silence"
        },
        {
          "id": 5,
          "label": "White uniform noise"
        },
        {
          "id": 6,
          "label": "Pink noise"
        },
        {
          "id": 7,
          "label": "Sine table"
        },
        {
          "id": 8,
          "label": "Periodic Ticks"
        },
        {
          "id": 9,
          "label": "White Gaussian noise"
        },
        {
          "id": 10,
          "label": "Red (brownian) noise"
        },
        {
          "id": 11,
          "label": "Blue noise"
        },
        {
          "id": 12,
          "label": "Violet noise"
        }
      ],
      "prop_id": 7,
      "order": 140,
      "parent": "",
      "enabled": true
    }
  ],
  "method": [],
  "kind": "audiotestsrc"
}
```
