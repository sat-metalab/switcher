/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "./initialized.hpp"

namespace switcher {
namespace gst {
Initialized::Initialized() {
  if (!gst_is_initialized()) gst_init(nullptr, nullptr);
  registry_ = gst_registry_get();
  // TODO add option for scanning a path
  gst_registry_scan_path(registry_, "/usr/local/lib/gstreamer-1.0/");
  gst_registry_scan_path(registry_, "/usr/lib/gstreamer-1.0/");
}

bool Initialized::set_plugin_as_primary(const std::string& plugin_name, int priority) {
  GstPluginFeature* plugin = gst_registry_lookup_feature(registry_, plugin_name.c_str());
  if (plugin != nullptr) {
    gst_plugin_feature_set_rank(plugin, GST_RANK_PRIMARY + priority);
    gst_object_unref(plugin);
    return true;
  }
  return false;
}

std::string Initialized::get_gstreamer_version() {
  guint major, minor, micro, nano;

  gst_version(&major, &minor, &micro, &nano);

  std::string nano_str = "";
  if (nano == 1)
    nano_str = "(GIT)";
  else if (nano == 2)
    nano_str = "(Prerelease)";

  std::string gstreamer_version_string = std::to_string(major) + "." + std::to_string(minor) + "." +
                                         std::to_string(micro) + " " + nano_str;

  return gstreamer_version_string;
}
}  // namespace gst
}  // namespace switcher
