/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SWITCHER_SWITCHER_H__
#define __SWITCHER_SWITCHER_H__

#include <cstdlib>
#include <filesystem>
#include <regex>
#include <string>
#include <unordered_set>
#include <vector>

#include "configuration/configurable.hpp"
#include "gst/initialized.hpp"
#include "infotree/information-tree.hpp"
#include "logger/logger.hpp"
#include "quiddity/container.hpp"
#include "quiddity/factory.hpp"
#include "quiddity/quid-id-t.hpp"
#include "session/session.hpp"
#include "utils/make-consultable.hpp"
#include "utils/string-utils.hpp"

namespace fs = std::filesystem;

namespace switcher {

// forward declarations
namespace quiddity::bundle {
class Bundle;
}  // namespace quiddity::bundle
namespace session {
class Session;
}  // namespace session

using namespace session;

class Switcher : public gst::Initialized,
                 public configuration::Configurable,
                 public logger::Logger {
  friend class quiddity::bundle::Bundle;  // access to qcontainer_ and qfactory_
 public:
  /**
   * @brief The name of the Switcher instance
   * @details A name is given in constructor parameters when `making` a Switcher instance
   */
  const std::string name_;

 public:
  /**
   * @brief The session of the Swicher instance
   * @details Every Switcher instance has its own session to manage session files to keep a
   *          history of the current state of a Switcher instance.
   */
  std::shared_ptr<Session> session_;

  /**
   * @brief Shared pointer type
   */
  using ptr = std::shared_ptr<Switcher>;

  static Switcher::ptr make_switcher(const std::string& name, bool debug = false);
  Switcher(const Switcher&) = delete;
  ~Switcher() = default;

  Switcher& operator=(const Switcher&) = delete;

  std::string get_switcher_version() const;
  std::string get_switcher_caps() const;

  int get_control_port() const;
  void set_control_port(const int port);

  /**
   * @brief This will serialize the state of every connection from a quiddity to another quiddity
   * or to a raw shmdata as an info tree.
   *
   * \return A shared pointer to an \ref switcher::InfoTree containing the connection
   * information. To see the format (serialized in json), go look at the "connections"
   * object in the wrapper/python/tests/session_raw.json file.
   */
  InfoTree::ptr get_connection_state() const;

  /**
   * This will serialize the state of every quiddities. The serialization is in the form of an \ref
   * switcher::InfoTree.
   *
   * this saves custom trees, the nickname and kind (except for the ones
   * present in the protected_quiddities_ unordered_set), the user tree,
   * the writable and savable properties and the state of all connections of every quiddity.
   * It also will call the on_saved callback of every quiddity.
   *
   * \return A shared pointer to an \ref switcher::InfoTree that contains all this information. To
   * see the format (serialized in json), go look at the wrapper/python/tests/session_raw.json file.
   */
  InfoTree::ptr get_state() const;

  /**
   * This takes a pointer to an info tree containing information in the format that
   * \ref switcher::get_state outputs and tries to get switcher back to that state.
   * It will create quiddities with the right nicknames, apply its serialized properties,
   * restore its user data tree, start it if need be, restore its connection
   * states and call its on_loaded function.
   *
   * Caution : quiddities that were in the protected_quiddities_ unordered_set at the moment
   * the state was serialized will not be created. load_state will assume they still
   * exist and try to restore them but will fail if they do not already exist.
   *
   * Caution : writer quiddities with dynamic swids that depend on external event
   * to be generated will still rely on these external events to happen before their swid
   * exist. This may caused some serialized connection to not be restored.
   *
   * Caution : this will try to reconnect quiddities connected to raw shmpath. The quiddity
   * may be left in a waiting state if the raw shmpath no longer exist and is not recreated.
   *
   * \param state pointer to an \ref switcher::InfoTree containing the state.
   * \return true if the function exits in a graceful state (some connection failures
   * are tolerated) and false if the passed \ref switcher::InfoTree is a nullptr.
   */
  bool load_state(InfoTree* state);

  /**
   * Removes all quiddities
   */
  void stop();

  /**
   * Removes all quiddities that are not stored in protected_quiddities_
   */
  void reset_state();

  /**
   * Add all currently existing quiddities ids to \ref Switcher::protected_quiddities_, making them
   * immune to removal by Switcher::reset_state()
   */
  void protect_quiddities();

  /**
   * Add the specified quiddity ids to \ref Switcher::protect_quiddities_, making them
   * immune to removal by Switcher::reset_state()
   */
  void protect_quiddities(std::vector<quiddity::qid_t> protected_ids);

  // Quiddity Factory
  Make_delegate(Switcher, quiddity::Factory, &qfactory_, factory);

  // Quiddity container
  Make_delegate(Switcher, quiddity::Container, qcontainer_.get(), quids);

  /**
   * Raw pointer to the quiddity container. Since qcontainer_ is a shared pointer and it lives until
   * the switcher dies, theres no risk to using this.
   */
  quiddity::Container* quiddities;

  // Configuration
  Make_delegate(Switcher, Configuration, &conf_, conf);

  // shmpaths
  const std::string get_shm_dir() const { return conf_.get_value(".shm.directory"); }
  const std::string get_shm_prefix() const { return conf_.get_value(".shm.prefix"); }

  // Bundles
  bool load_bundle_from_config(const std::string& bundle_description);

  bool register_bundle(InfoTree::ptr bundle_tree);
  bool unregister_bundle(InfoTree::ptr bundle_tree);

 private:
  /**
   * @brief Deletes the default constructor
   */
  Switcher() = delete;

  /**
   * @brief Constructor
   *
   * @param switcher_name The name of the Switcher instance
   * @param debug A boolean that indicates if a `debug` log level should be enforced
   */
  Switcher(const std::string& switcher_name, bool debug = false);

  /**
   * Scan a directory for plugins and load them. The scan is recursive.
   *
   * @param path the path of the directory
   */
  void scan_dir_for_plugins(const std::string& path);

  void remove_shm_zombies() const;
  void apply_gst_configuration();
  void register_bundle_from_configuration();
  static void init_gst();

  quiddity::Factory qfactory_;
  quiddity::Container::ptr qcontainer_;
  /**
   * std::unordered_set of quiddity ids that were kept after a call to reset_state.
   */
  std::unordered_set<quiddity::qid_t> protected_quiddities_{};
  std::weak_ptr<Switcher> me_{};
  int control_port_{0};
};
}  // namespace switcher

#endif
