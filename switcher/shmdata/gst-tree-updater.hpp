/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SWITCHER_GST_SHM_TREE_UPDATER_H__
#define __SWITCHER_GST_SHM_TREE_UPDATER_H__

#include "./direction.hpp"
#include "./gst-subscriber.hpp"

namespace switcher {
namespace shmdata {

/**
 * A `gstreamer` helper that updates its InfoTree when it is extended.
 **/
class GstTreeUpdater {
 public:
  /**
   * A callback that is triggers when caps are added or updated.
   * \see [All references to this
   *callback](https://gitlab.com/search?group_id=57437697&project_id=2101216&scope=blobs&search=on_caps_cb_t)
   **/
  using on_caps_cb_t = std::function<void(const std::string&)>;

  /**
   * A callback that is triggers when some caps are deleted.
   * \see [All references to this
   *callback](https://gitlab.com/search?group_id=57437697&project_id=2101216&scope=blobs&search=on_delete_t)
   **/
  using on_delete_t = std::function<void()>;

  /**
   * GstTreeUpdater constructor for shmdata readers. This will update the shmdata reader part
   * of the info tree of a quiddity that uses gstreamer.
   * \param quid the quiddity
   * \param element the gstreamer element
   * \param shmpath the shmdata path
   * \param sfid the sfid of the connection to the quiddity
   * \param on_caps_cb_t callback when caps are updated. Its left to its default value in a lot of
   * cases. \param on_delete_t callback when caps are deleted deleted. Its left to its default value
   * in a lot of cases.
   */
  GstTreeUpdater(quiddity::Quiddity* quid,
                 GstElement* element,
                 const std::string& shmpath,
                 switcher::quiddity::claw::sfid_t sfid,
                 on_caps_cb_t on_caps_cb = nullptr,
                 on_delete_t on_delete_cb = nullptr);

  /**
   * GstTreeUpdater constructor for shmdata reader or writer. This will update the shmdata reader or
   * writer part of the info tree of a quiddity that uses gstreamer. This is also used for objects
   * that are not quiddities but that still need to interact with gstreamer \param quid the quiddity
   * \param element the gstreamer element
   * \param shmpath the shmdata path
   * \param dir wether the GstTreeUpdater is associated with a writer or reader quiddity
   * \param on_caps_cb_t callback when caps are updated. Its left to its default value in a lot of
   * cases. \param on_delete_t callback when it is deleted. Its left to its default value in a lot
   * of cases. \param sfid sfid of the connection. This may be 0 if the dir is writer or if its
   * associated with a reader that is not a quiddity.
   */
  GstTreeUpdater(quiddity::Quiddity* quid,
                 GstElement* element,
                 const std::string& shmpath,
                 Direction dir,
                 on_caps_cb_t on_caps_cb = nullptr,
                 on_delete_t on_delete_cb = nullptr,
                 switcher::quiddity::claw::sfid_t sfid = 0);

  ~GstTreeUpdater();
  GstTreeUpdater() = delete;

 private:
  quiddity::Quiddity* quid_;
  std::string shmpath_;
  Direction dir_;
  std::string key_;
  on_delete_t on_del_;
  GstSubscriber shm_sub_;
};

}  // namespace shmdata
}  // namespace switcher
#endif
