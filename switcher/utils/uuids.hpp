/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SWITCHER_UTILS_UUIDS_H__
#define __SWITCHER_UTILS_UUIDS_H__

#include <uuid/uuid.h>

#include <string>

namespace switcher {
class UUIDs {
 public:
  using id_t = std::string;
  /**
   * value of an invalid uuid.
   * k is google C++ style for "constant"
   */
  static const id_t kInvalid;
  /**
   * Creates a new uuid V4 identifier.
   * this is unique enough for us.
   * see https://en.wikipedia.org/w/index.php?title=Universally_unique_identifier#Collisions
   * \return The new identifier.
   */
  static UUIDs::id_t make_uuid();
};
}  // namespace switcher
#endif
