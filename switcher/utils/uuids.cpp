/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "./uuids.hpp"

namespace switcher {

const UUIDs::id_t UUIDs::kInvalid = std::string();

// shamelessly copy pasted from the implementation in the logs.
// anyways, I think the logs doesn't use this anymore.
UUIDs::id_t UUIDs::make_uuid() {
  // declare structure to hold uuid in binary representation
  uuid_t binary_uuid;
  // generate a random binary uuid
  uuid_generate_random(binary_uuid);
  // declare variable to hold uuid in characters representation
  char uuid_chars[37] = {};
  // convert uuid binary to character representation
  uuid_unparse_lower(binary_uuid, uuid_chars);
  // cast char array to id_t, AKA string
  return UUIDs::id_t(uuid_chars, 36);
}
}  // namespace switcher
