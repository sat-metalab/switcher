/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "./basic-test.hpp"

#include <iostream>

namespace switcher {
namespace quiddity {
bool test::full(Switcher::ptr manager, const std::string& quiddity_kind, InfoTree::ptr config) {
  if (!create(manager, quiddity_kind, config)) return false;
  if (!tree(manager, quiddity_kind, config)) return false;
  if (!startable(manager, quiddity_kind, config)) return false;
  if (!properties(manager, quiddity_kind, config)) return false;
  return true;
}

bool test::create(Switcher::ptr manager, const std::string& quiddity_kind, InfoTree::ptr config) {
  {
    auto res = manager->quiddities->create(quiddity_kind, quiddity_kind, config.get());
    if (!res) {
      manager->sw_error("{} cannot be created: {}", quiddity_kind, res.msg());
      return false;
    }
    if (res.msg() != quiddity_kind) {
      manager->sw_error("{} was created with the wrong name", quiddity_kind);
      return false;
    }
    auto res_rm = manager->quids<MPtr(&quiddity::Container::remove)>(
        manager->quids<MPtr(&quiddity::Container::get_id)>(quiddity_kind));
    if (!res_rm) {
      manager->sw_error("error while removing quiddity {}: {}", quiddity_kind, res.msg());
      return false;
    }
  }
  {  // testing with generated name
    auto res = manager->quiddities->create(quiddity_kind, std::string(), config.get());
    if (!res) {
      manager->sw_error("{}  cannot be created: {}", quiddity_kind, res.msg());
      return false;
    }
    if (res.msg().empty()) {
      manager->sw_error("creation did not generate a name for class {}", quiddity_kind);
      return false;
    }
    auto res_rm = manager->quids<MPtr(&quiddity::Container::remove)>(
        manager->quids<MPtr(&quiddity::Container::get_id)>(res.msg()));
    if (!res_rm) {
      manager->sw_error("error while removing quiddity: {}", res.msg());
      return false;
    }
  }
  return true;
}

bool test::startable(Switcher::ptr manager,
                     const std::string& quiddity_kind,
                     InfoTree::ptr config) {
  auto qrox = manager->quiddities->create(quiddity_kind, quiddity_kind, config.get());
  if (!qrox) {
    manager->sw_error("{} cannot be created (startable not actually tested)", quiddity_kind);
    return false;
  }
  auto started_id = qrox.get()->prop<MPtr(&property::PBag::get_id)>("started");
  if (0 != started_id) {
    qrox.get()->prop<MPtr(&property::PBag::set<bool>)>(started_id, true);
    qrox.get()->prop<MPtr(&property::PBag::set<bool>)>(started_id, false);
    qrox.get()->prop<MPtr(&property::PBag::set<bool>)>(started_id, true);
  }
  if (!manager->quids<MPtr(&quiddity::Container::remove)>(qrox.get_id())) {
    manager->sw_error("error while removing quiddity {} (startable test)", quiddity_kind);
    return false;
  }
  return true;
}

bool test::tree(Switcher::ptr manager, const std::string& quiddity_kind, InfoTree::ptr config) {
  auto qrox = manager->quiddities->create(quiddity_kind, std::string(), config.get());
  if (!qrox) {
    manager->sw_error("{} cannot be created (tree not actually tested)");
    return false;
  }
  qrox.get()->tree<MPtr(&InfoTree::serialize_json)>(".");
  manager->quids<MPtr(&quiddity::Container::remove)>(qrox.get_id());
  return true;
}

bool test::properties(Switcher::ptr manager,
                      const std::string& quiddity_kind,
                      InfoTree::ptr config) {
  auto qrox = manager->quiddities->create(quiddity_kind, std::string(), config.get());
  if (!qrox) {
    manager->sw_error("{} cannot be created (properties not actually tested)", quiddity_kind);
    return false;
  }
  auto quid = qrox.get();
  auto properties = quid->tree<MPtr(&InfoTree::get_child_keys)>(".property.");
  for (auto& it : properties) {
    // do not test if the property has been disabled
    if (quid->tree<MPtr(&InfoTree::branch_read_data<bool>)>(std::string(".property.") + it +
                                                            ".enabled.")) {
      auto default_value = quid->prop<MPtr(&property::PBag::get_str_str)>(it);
      if (!default_value.empty() && quid->tree<MPtr(&InfoTree::branch_read_data<bool>)>(
                                        std::string(".property.") + it + ".writable.")) {
        if (!quid->prop<MPtr(&property::PBag::set_str_str)>(it, default_value)) {
          manager->sw_error(
              "property {} for quiddity of class {} cannot be set with its default value",
              it,
              quiddity_kind);
          return false;
        }
      }
    }
  }
  return true;
}

}  // namespace quiddity
}  // namespace switcher
