/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "./container.hpp"

#include <algorithm>

#include "../shmdata/caps/utils.hpp"
#include "../switcher.hpp"
#include "../utils/scope-exit.hpp"

namespace switcher {
namespace quiddity {
Container::ptr Container::make_container(Switcher* switcher, Factory* factory) {
  Container::ptr container(new Container(switcher, factory));
  container->me_ = container;
  container->logger_ = switcher;
  return container;
}

Container::Container(Switcher* switcher, Factory* factory)
    : factory_(factory), switcher_(switcher) {}

Qrox Container::create(const std::string& quiddity_kind,
                       const std::string& nickname,
                       InfoTree::ptrc override_config,
                       quiddity::qid_t quiddity_id) {
  // create a quiddity qrox
  auto res = quiet_create(quiddity_kind, nickname, override_config, quiddity_id);
  // skip signals if creation failed
  if (!res) return res;

  // Copy the registered callbacks map
  // @NOTE: This is required in case a callback modifies it
  auto on_created_map = on_created_cbs_;
  // Iterate over callbacks
  for (auto& pair : on_created_map) {
    // Call second element of the pair which is the callback,
    // passing the current quiddity id as an argument.
    pair.second(res.get_id());
    if (on_created_cbs_.empty()) break;
  }
  // return the qrox
  return res;
}

/**
 * @brief Find next nickname index available for a given kind
 *
 * @param quiddity_kind  Quiddity kind.
 * @returns              Nickname index for kind.
 */
size_t Container::find_next_kind_index(const std::string& quiddity_kind) const {
  size_t kind_index = 1;
  bool found_free_index = false;
  const std::set<std::string> nicknames = get_nicknames();
  // try every nickname+index combo until we find one that is not in use
  while (!found_free_index) {
    const std::string nickname = quiddity_kind + std::to_string(kind_index);
    if (auto search = nicknames.find(nickname); search != nicknames.end())
      ++kind_index;
    else
      found_free_index = true;
  }
  return kind_index;
};

Qrox Container::quiet_create(const std::string& quiddity_kind,
                             const std::string& raw_nickname,
                             InfoTree::ptrc override_config,
                             quiddity::qid_t quiddity_id) {
  // checks before creation
  if (!switcher_->factory<MPtr(&Factory::exists)>(quiddity_kind)) {
    return Qrox(false, "unknown Quiddity kind");
  }

  if (quiddity_id == UUIDs::kInvalid) {
    // If we got an invalid id as argument (the default argument of the function), generate a new ID
    quiddity_id = UUIDs::make_uuid();
  }
  auto it = quiddities_.find(quiddity_id);
  if (it != quiddities_.end()) {
    // Provided there are no bugs, this can happen in two cases :
    // A session file was loaded twice, or was manually edited to contain the id of a quiddity that
    // exists in the current switcher OR, you were extremely unlucky and there was a UUID V4
    // collision.

    // In that case, assume that the already existing quiddity is the same as the one we wanted to
    // create and return it.
    logger_->sw_warning("Quiddity with ID {} already exists, returning the existing quiddity.",
                        quiddity_id);
    return get_qrox(quiddity_id);
  }

  // nickname
  std::string nick;
  if (raw_nickname.empty()) {
    nick = quiddity_kind + std::to_string(find_next_kind_index(quiddity_kind));
  } else {
    nick = raw_nickname;
    for (const auto& it : quiddities_) {
      if (nick == it.second->get_nickname()) {
        return Qrox(false, "nickname unavailable");
      }
    }
  }

  // building configuration for quiddity creation
  InfoTree::ptr tree;
  auto conf = switcher_->conf<MPtr(&Configuration::get)>();
  if (conf) {
    tree = conf->get_tree(quiddity_kind);
    if (tree->empty()) tree = conf->get_tree("bundle." + quiddity_kind);
  }

  // creation
  Quiddity::ptr quiddity =
      factory_->create(quiddity_kind,
                       Config(quiddity_id,
                              nick,
                              quiddity_kind,
                              InfoTree::merge(tree.get(), override_config).get(),
                              this));
  if (!quiddity) {
    return Qrox(false, "Quiddity creation error");
  }

  if (!(*quiddity.get())) {
    return Qrox(false, "Quiddity initialization error");
  }
  // insert the id in our vector of current ids
  ids_.push_back(quiddity_id);
  quiddities_[quiddity_id] = quiddity;

  return Qrox(true, nick, quiddity_id, quiddity.get());
}

/**
 * @brief Send confirmation signal for quiddity creation.
 *        Use after a Container::quiet_create call.
 *
 * @param quid The quiddity instance returned by Container::quiet_create.
 */
void Container::notify_quiddity_created(Quiddity* quid) {
  // We work on a copy in case a callback modifies the map of registered callbacks
  auto cbs_map = on_created_cbs_;
  for (auto& pair : cbs_map) {
    auto callback = pair.second;
    callback(quid->get_id());
    if (on_created_cbs_.empty()) break;  // In case the map gets reset in the callback, e.g bundle
  }
};

BoolLog Container::remove(qid_t id) {
  // We work on a copy in case a callback modifies the map of registered callbacks
  const auto tmp_removed_cbs_ = on_removed_cbs_;
  for (const auto& cb : tmp_removed_cbs_) {
    cb.second(id);
    if (on_removed_cbs_.empty()) break;  // In case the map gets reset in the callback, e.g. bundle
  }

  std::shared_ptr<Quiddity> quiddity_to_remove;
  auto it = quiddities_.find(id);
  if (it == quiddities_.end()) {
    return BoolLog(false, "quiddity not found");
  }
  quiddity_to_remove = it->second;

  // disconnect all followers that follow this quiddities writers.
  for (const auto& swid : quiddity_to_remove->claw<MPtr(&quiddity::claw::Claw::get_swids)>()) {
    std::vector<quiddity::claw::Claw::WriterConnection> connections =
        quiddity_to_remove->claw<MPtr(&quiddity::claw::Claw::get_registered_writer_connections)>(
            swid);
    // for every writer connection registered, find the follower and disconnect the sfid
    // connected to a swid of the quiddity_to_remove.
    for (const quiddity::claw::Claw::WriterConnection& connection : connections) {
      std::shared_ptr<Quiddity> quiddity_to_disconnect;
      auto it = quiddities_.find(connection.follower_quiddity_id);
      if (it == quiddities_.end()) {
        continue;
      }
      quiddity_to_disconnect = it->second;
      // disconnect the sfid. This will also unregister the WriterConnection
      quiddity_to_disconnect->claw<MPtr(&quiddity::claw::Claw::disconnect)>(
          connection.connected_sfid);
    }
  }
  // disconnect all the followers of this quiddity.
  for (const auto& sfid : quiddity_to_remove->claw<MPtr(&quiddity::claw::Claw::get_sfids)>()) {
    // checking for empty shmpath is the easiest way to know if the sfid is connected to anything.
    if (!quiddity_to_remove->claw<MPtr(&quiddity::claw::Claw::get_follower_shmpath)>(sfid)
             .empty()) {
      quiddity_to_remove->claw<MPtr(&quiddity::claw::Claw::disconnect)>(sfid);
    }
  }

  auto res = quiet_remove(id);
  if (!res) return res;
  return res;
}

BoolLog Container::quiet_remove(qid_t id) {
  auto it = quiddities_.find(id);
  if (it == quiddities_.end()) {
    return BoolLog(false, "quiddity not found");
  }
  quiddities_.erase(id);

  ids_.erase(
      std::remove_if(ids_.begin(), ids_.end(), [&](qid_t quid_id) { return id == quid_id; }));

  return BoolLog(true);
}

std::set<std::string> Container::get_nicknames() const {
  std::set<std::string> res;
  for (const auto& it : quiddities_) res.insert(it.second->get_nickname());
  return res;
}

std::vector<qid_t> Container::get_ids() const { return ids_; }

InfoTree::ptr Container::get_quiddities_description() {
  auto tree = InfoTree::make();
  tree->graft("quiddities", InfoTree::make());
  tree->tag_as_array("quiddities", true);
  auto subtree = tree->get_tree("quiddities");
  for (const auto& it : quiddities_) {
    if (it.second) {
      auto quid = it.second;
      const auto id = quid->get_id();
      subtree->graft(id + ".id", InfoTree::make(quid->get_id()));
      subtree->graft(id + ".kind", InfoTree::make(quid->get_kind()));
      subtree->graft(id + ".nickname", InfoTree::make(quid->get_nickname()));
    }
  }
  return tree;
}

InfoTree::ptr Container::get_quiddity_description(qid_t id) {
  auto it = quiddities_.find(id);
  if (quiddities_.end() == it) return InfoTree::make();
  return it->second->get_description();
}

Quiddity::ptr Container::get_quiddity(qid_t id) {
  // use with care, this will return a shared_ptr. Use only when you want to own the quiddity.
  auto it = quiddities_.find(id);
  if (quiddities_.end() == it) {
    logger_->sw_debug("quiddity {} not found, cannot provide ptr", id);
    Quiddity::ptr empty_quiddity_ptr;
    return empty_quiddity_ptr;
  }
  return it->second;
}

unsigned int Container::register_creation_cb(void_func_t cb) {
  static unsigned int id = 0;
  id %= std::numeric_limits<unsigned int>::max();
  me_.lock();
  on_created_cbs_[++id] = cb;
  return id;
}

unsigned int Container::register_removal_cb(void_func_t cb) {
  static unsigned int id = 0;
  id %= std::numeric_limits<unsigned int>::max();
  me_.lock();
  on_removed_cbs_[++id] = cb;
  return id;
}

void Container::unregister_creation_cb(unsigned int id) {
  me_.lock();
  auto it = on_created_cbs_.find(id);
  if (it != on_created_cbs_.end()) on_created_cbs_.erase(it);
}

void Container::unregister_removal_cb(unsigned int id) {
  me_.lock();
  auto it = on_removed_cbs_.find(id);
  if (it != on_removed_cbs_.end()) on_removed_cbs_.erase(it);
}

void Container::reset_create_remove_cb() {
  me_.lock();
  on_created_cbs_.clear();
  on_removed_cbs_.clear();
}

qid_t Container::get_id(const std::string& nickname) const {
  for (const auto& it : quiddities_) {
    if (nickname == it.second->get_nickname()) return it.first;
  }
  // not found
  return UUIDs::kInvalid;
}

std::string Container::get_nickname(qid_t id) const {
  return quiddities_.find(id)->second->get_nickname();
}

Qrox Container::get_qrox(qid_t id) {
  auto quiddity_pointer = get_quiddity(id);
  if (!quiddity_pointer) {
    return Qrox(false, "Quiddity not found");
  }
  return Qrox(true, get_nickname(id), id, quiddity_pointer.get());
}

}  // namespace quiddity
}  // namespace switcher
