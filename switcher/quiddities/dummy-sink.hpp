/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SWITCHER_DUMMY_SINK_H__
#define __SWITCHER_DUMMY_SINK_H__

#include "../quiddity/quiddity.hpp"
#include "../shmdata/follower.hpp"

namespace switcher {
namespace quiddities {
using namespace quiddity;
class DummySink : public Quiddity {
 public:
  DummySink(quiddity::Config&&);

 private:
  static const std::string kConnectionSpec;  //!< Shmdata specifications
  /**
   * internal value of the frame_received_ property. Initially false
   */
  bool frame_received_{false};
  /**
   * internal value of the connect_called_ property. Initially false
   */
  bool connect_called_{false};
  /**
   * internal value of the disconnect_called_ property. Initially false
   */
  bool disconnect_called_{false};
  /**
   * Id of a \ref PropertyBase that is true or false depending on if the on_data
   * callback of the Follower generated in the connect method of this quiddity
   * was called since this was last reset to false by the user.
   * Used in tests to verify that a quiddity connected to this really sends data
   * and that this quiddity can receive it.
   */
  property::prop_id_t frame_received_id_;
  /**
   * Id of a \ref PropertyBase that is true or false depending on if the connect method
   * was called since this was last reset to false by the user.
   * Used in tests to verify how many times the connect method is called
   * on a quiddity
   */
  property::prop_id_t connect_called_id_;
  /**
   * Id of a \ref PropertyBase that is true or false depending on if the disconnect method
   * was called since this was last reset to false by the user.
   * Used in tests to verify how many times the disconnect method is called
   * on a quiddity
   */
  property::prop_id_t disconnect_called_id_;

  // shmdata follower
  std::unique_ptr<shmdata::Follower> shm_{nullptr};
  /**
   * Called by the on_connect_cb_ of this quiddity. Creates
   * a \ref Follower for the connected shmdata_path and sets its on_data
   * callback to a function that sets the frame_received property to true.
   * A pointer to the Follower is stored in shm_.
   *
   * This also sets the connect-called property to true and notifies that the property changed.
   *
   * \param shmdata_path the path of the shmdata this quiddity was connected with
   * \return true if everything worked and false if something went wrong. There are no
   * explicit false return in the function.
   */
  bool connect(const std::string& shmdata_path);
  /**
   * Called by the on_disconnect_cb_ of this quiddity. Resets the shm_ pointer, setting
   * it to nullptr.
   *
   * This also sets the disconnect-called property to true and notifies that the property changed.
   * \return true if everything worked and false if something went wrong. There are no
   * explicit false return in the function.
   */
  bool disconnect();
};

}  // namespace quiddities
}  // namespace switcher
#endif
