/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SWITCHER_GST_VIDEO_ENCODER_H__
#define __SWITCHER_GST_VIDEO_ENCODER_H__

#include <memory>

#include "../gst/video-codec.hpp"
#include "../quiddity/quiddity.hpp"

namespace switcher {
namespace quiddities {
using namespace quiddity;

/**
 * The GstVideoEncoder class is wrapping gstreamer in order to re-encodes video streams with the
 *available video codecs.
 **/
class GstVideoEncoder : public Quiddity {
 public:
  GstVideoEncoder(quiddity::Config&&);

 private:
  static const std::string kConnectionSpec;  //!< Shmdata specifications
  std::unique_ptr<gst::VideoCodec> codecs_{nullptr};

  /**
   * Calls the stop method of the codec when something is disconnected from the quiddity
   * \return A boolean asserting that a shmdata is disconnected. A false value is aborting the
   * disconnection.
   */
  bool on_shmdata_disconnect();

  /**
   * This is called when a connection is made to this quiddity. Sets the shmpath of the video codec
   and calls its start method. The video codec does all the job of managing the info tree of the
   quiddity.
   * \param shmpath path of the shmdata.
   * \param sfid sfid of the connection.
   * \return A boolean asserting that a shmdata is connected. A false value is aborting the
   connection.
   */
  bool on_shmdata_connect(const std::string& shmdata_socket_path, claw::sfid_t sfid);
};

}  // namespace quiddities
}  // namespace switcher
#endif
