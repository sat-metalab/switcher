/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "./external-shmdata-writer.hpp"

#include "../infotree/json-serializer.hpp"

namespace switcher {
namespace quiddities {
SWITCHER_MAKE_QUIDDITY_DOCUMENTATION(ExternalWriter,
                                     "extshmsrc",
                                     "Raw Shmdata",
                                     "Import an external shmdata writer",
                                     "LGPL",
                                     "Nicolas Bouillot");

const std::string ExternalWriter::kConnectionSpec(R"(
{
"writer":
  [
    {
      "label": "custom",
      "description": "external shmdata",
      "can_do": ["all"]
    }
  ]
}
)");

void ExternalWriter::override_claw(const std::string& shmpath) {
  // We reconstruct claw_ in order to have access to its forced_writer_shmpaths_ map.
  claw_ = claw::Claw(this, claw::ConnectionSpec(kConnectionSpec), nullptr, nullptr);
  // Adds an association from the swid 1 (ext_shmsrc_swid_) to the shmdata_path_ to the
  // forced_writer_shmpaths_ map. We do this because an External writer
  // doesn't create its own shmdata : it only exposes an already existing one.
  // If we were not to override it, the connect method of the claw would
  // generate a shmdata path using this quiddity's internal id and the
  // result would always be wrong.
  claw_.forced_writer_shmpaths_.emplace(ext_shmsrc_swid_, shmpath);
}

ExternalWriter::ExternalWriter(quiddity::Config&& conf)
    : Quiddity(std::forward<quiddity::Config>(conf), {kConnectionSpec}) {
  pmanage<MPtr(&property::PBag::make_string)>(
      "shmdata-path",
      [this](const std::string& val) {
        shmdata_path_ = val;
        this->override_claw(val);
        // This is a Follower with Direction::writer because it
        // does not generate its own shmdata, rather, it follows
        // a shmdata (usually from the PJSip plugin) but exposes it as if it were a writer
        shm_ = std::make_unique<shmdata::Follower>(
            this,
            shmdata_path_,
            nullptr,  // on data
            [this](const std::string& data_type) {
              return on_shmdata_connected(data_type);
            },        // on server connected
            nullptr,  // on server disconnected
            shmdata::Stat::kDefaultUpdateInterval,
            shmdata::Direction::writer);
        return true;
      },
      [this]() { return shmdata_path_; },
      "Shmdata Path",
      "Path Of The Shmdata",
      "");
}

InfoTree::ptr ExternalWriter::on_saving() {
  return infotree::json::deserialize(tree<MPtr(&InfoTree::serialize_json)>(".shmdata.writer."));
}

void ExternalWriter::on_loading(InfoTree::ptr&& tree) {
  if (tree->empty()) return;
  graft_tree(".shmdata.writer.", tree);
}

bool ExternalWriter::on_shmdata_connected(const std::string& data_type) {
  // the mimetype is always the part of the caps before the first comma
  std::string mime_type(data_type.begin(), std::find(data_type.begin(), data_type.end(), (',')));
  // replace the can do of our swid to exactly reflect the type of the data transmitted by the
  // wrapped shmdata.
  claw_.replace_writer_can_do(ext_shmsrc_swid_,
                              std::vector<::shmdata::Type>{::shmdata::Type(mime_type)});
  return true;
}

}  // namespace quiddities
}  // namespace switcher
